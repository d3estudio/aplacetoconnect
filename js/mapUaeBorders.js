windowMap.prototype.createBorders = function() {
    // Made using http://www.birdtheme.org/useful/v3tool.html

    // Coordinates
    var dubaiCoordinates = [
        new google.maps.LatLng(25.356437, 55.299683),
        new google.maps.LatLng(25.289405, 55.458984),
        new google.maps.LatLng(25.237243, 55.604553),
        new google.maps.LatLng(25.217366, 55.656738),
        new google.maps.LatLng(24.978589, 55.667725),
        new google.maps.LatLng(24.799202, 55.736389),
        new google.maps.LatLng(24.781747, 55.725403),
        new google.maps.LatLng(24.746831, 55.739136),
        new google.maps.LatLng(24.734359, 55.733643),
        new google.maps.LatLng(24.696934, 55.601807),
        new google.maps.LatLng(24.634535, 55.524902),
        new google.maps.LatLng(24.627045, 55.365601),
        new google.maps.LatLng(24.599577, 55.313416),
        new google.maps.LatLng(24.649513, 55.263977),
        new google.maps.LatLng(24.671978, 55.258484),
        new google.maps.LatLng(24.679466, 55.145874),
        new google.maps.LatLng(24.851550, 55.030518),
        new google.maps.LatLng(24.903876, 54.887695),
        new google.maps.LatLng(24.941238, 54.755859),
        new google.maps.LatLng(25.391179, 55.162354),
        new google.maps.LatLng(25.356437, 55.299683)
    ];

    var sharjahEastCoordinates = [
        new google.maps.LatLng(25.103010, 56.366730),
        new google.maps.LatLng(25.095549, 56.324158),
        new google.maps.LatLng(24.971120, 56.320038),
        new google.maps.LatLng(24.923804, 56.324158),
        new google.maps.LatLng(24.912595, 56.296692),
        new google.maps.LatLng(24.935012, 56.278839),
        new google.maps.LatLng(24.931276, 56.179962),
        new google.maps.LatLng(24.926295, 56.181335),
        new google.maps.LatLng(24.883945, 56.278839),
        new google.maps.LatLng(24.881453, 56.306305),
        new google.maps.LatLng(24.896402, 56.328278),
        new google.maps.LatLng(24.932521, 56.354370),
        new google.maps.LatLng(24.968630, 56.328278),
        new google.maps.LatLng(24.967385, 56.344757),
        new google.maps.LatLng(24.973610, 56.352997),
        new google.maps.LatLng(24.978589, 56.385956),
        new google.maps.LatLng(25.103010, 56.366730)
    ];

    var dubaiEastCoordinates = [
        new google.maps.LatLng(24.836596, 56.210175),
        new google.maps.LatLng(24.782994, 56.207428),
        new google.maps.LatLng(24.731864, 56.111298),
        new google.maps.LatLng(24.740595, 56.078339),
        new google.maps.LatLng(24.877716, 55.975342),
        new google.maps.LatLng(24.898894, 55.979462),
        new google.maps.LatLng(24.875224, 56.053619),
        new google.maps.LatLng(24.868994, 56.064606),
        new google.maps.LatLng(24.864011, 56.061859),
        new google.maps.LatLng(24.842827, 56.103058),
        new google.maps.LatLng(24.836596, 56.210175)
    ];

    var abuDhabiCoordinates = [
        new google.maps.LatLng(25.015929, 54.791565),
        new google.maps.LatLng(24.913840, 54.890442),
        new google.maps.LatLng(24.856534, 55.044250),
        new google.maps.LatLng(24.686952, 55.154114),
        new google.maps.LatLng(24.686952, 55.258484),
        new google.maps.LatLng(24.671978, 55.272217),
        new google.maps.LatLng(24.609566, 55.313416),
        new google.maps.LatLng(24.639528, 55.362854),
        new google.maps.LatLng(24.642024, 55.522156),
        new google.maps.LatLng(24.709410, 55.593567),
        new google.maps.LatLng(24.739348, 55.711670),
        new google.maps.LatLng(24.759302, 55.722656),
        new google.maps.LatLng(24.786735, 55.711670),
        new google.maps.LatLng(24.796708, 55.725403),
        new google.maps.LatLng(24.806681, 55.763855),
        new google.maps.LatLng(24.799202, 55.791321),
        new google.maps.LatLng(24.806681, 55.813293),
        new google.maps.LatLng(24.799202, 55.827026),
        new google.maps.LatLng(24.776760, 55.838013),
        new google.maps.LatLng(24.726875, 55.832520),
        new google.maps.LatLng(24.669482, 55.843506),
        new google.maps.LatLng(24.634535, 55.805054),
        new google.maps.LatLng(24.614560, 55.821533),
        new google.maps.LatLng(24.567108, 55.774841),
        new google.maps.LatLng(24.527135, 55.772095),
        new google.maps.LatLng(24.482149, 55.794067),
        new google.maps.LatLng(24.442149, 55.827026),
        new google.maps.LatLng(24.417142, 55.832520),
        new google.maps.LatLng(24.409639, 55.840759),
        new google.maps.LatLng(24.324574, 55.840759),
        new google.maps.LatLng(24.304550, 55.813293),
        new google.maps.LatLng(24.269501, 55.794067),
        new google.maps.LatLng(24.251973, 55.766602),
        new google.maps.LatLng(24.236947, 55.761108),
        new google.maps.LatLng(24.236947, 55.783081),
        new google.maps.LatLng(24.219414, 55.802307),
        new google.maps.LatLng(24.204385, 55.835266),
        new google.maps.LatLng(24.219414, 55.862732),
        new google.maps.LatLng(24.221919, 55.958862),
        new google.maps.LatLng(24.216910, 55.967102),
        new google.maps.LatLng(24.186847, 55.978088),
        new google.maps.LatLng(24.164296, 55.969849),
        new google.maps.LatLng(24.119182, 56.005554),
        new google.maps.LatLng(24.091604, 56.022034),
        new google.maps.LatLng(24.066528, 56.022034),
        new google.maps.LatLng(24.059004, 56.011047),
        new google.maps.LatLng(24.038939, 55.906677),
        new google.maps.LatLng(24.006326, 55.838013),
        new google.maps.LatLng(24.018871, 55.799561),
        new google.maps.LatLng(24.048972, 55.777588),
        new google.maps.LatLng(24.043956, 55.758362),
        new google.maps.LatLng(24.051480, 55.730896),
        new google.maps.LatLng(23.988762, 55.593567),
        new google.maps.LatLng(23.978724, 55.577087),
        new google.maps.LatLng(23.941076, 55.491943),
        new google.maps.LatLng(23.923502, 55.505676),
        new google.maps.LatLng(23.908438, 55.505676),
        new google.maps.LatLng(23.888349, 55.522156),
        new google.maps.LatLng(23.875792, 55.530396),
        new google.maps.LatLng(23.848162, 55.541382),
        new google.maps.LatLng(23.823039, 55.535889),
        new google.maps.LatLng(23.797911, 55.541382),
        new google.maps.LatLng(23.755182, 55.541382),
        new google.maps.LatLng(23.725012, 55.574341),
        new google.maps.LatLng(23.629427, 55.579834),
        new google.maps.LatLng(23.581609, 55.544128),
        new google.maps.LatLng(23.548881, 55.530396),
        new google.maps.LatLng(23.526218, 55.505676),
        new google.maps.LatLng(23.453168, 55.453491),
        new google.maps.LatLng(23.392682, 55.437012),
        new google.maps.LatLng(23.375035, 55.420532),
        new google.maps.LatLng(23.380077, 55.404053),
        new google.maps.LatLng(23.168139, 55.285950),
        new google.maps.LatLng(23.110049, 55.239258),
        new google.maps.LatLng(23.079732, 55.236511),
        new google.maps.LatLng(23.064570, 55.231018),
        new google.maps.LatLng(23.046880, 55.236511),
        new google.maps.LatLng(23.024132, 55.225525),
        new google.maps.LatLng(22.998852, 55.228271),
        new google.maps.LatLng(22.978624, 55.225525),
        new google.maps.LatLng(22.958393, 55.225525),
        new google.maps.LatLng(22.928042, 55.220032),
        new google.maps.LatLng(22.905273, 55.225525),
        new google.maps.LatLng(22.885032, 55.231018),
        new google.maps.LatLng(22.859726, 55.228271),
        new google.maps.LatLng(22.844540, 55.228271),
        new google.maps.LatLng(22.824289, 55.233765),
        new google.maps.LatLng(22.809099, 55.228271),
        new google.maps.LatLng(22.793907, 55.231018),
        new google.maps.LatLng(22.710323, 55.220032),
        new google.maps.LatLng(22.624152, 55.140381),
        new google.maps.LatLng(22.626687, 55.110168),
        new google.maps.LatLng(22.720457, 54.343872),
        new google.maps.LatLng(22.930571, 52.577820),
        new google.maps.LatLng(24.124195, 51.583557),
        new google.maps.LatLng(24.472150, 51.564331),
        new google.maps.LatLng(25.015929, 54.791565)
    ];

    var sharjahCoordinates = [
        new google.maps.LatLng(24.801695, 55.814667),
        new google.maps.LatLng(24.792968, 55.791321),
        new google.maps.LatLng(24.797955, 55.762482),
        new google.maps.LatLng(24.791722, 55.728149),
        new google.maps.LatLng(24.982324, 55.658112),
        new google.maps.LatLng(25.053257, 55.663605),
        new google.maps.LatLng(25.090574, 55.659485),
        new google.maps.LatLng(25.211154, 55.653992),
        new google.maps.LatLng(25.254633, 55.516663),
        new google.maps.LatLng(25.350231, 55.284576),
        new google.maps.LatLng(25.542441, 55.427399),
        new google.maps.LatLng(25.569698, 55.487823),
        new google.maps.LatLng(25.508982, 55.523529),
        new google.maps.LatLng(25.433353, 55.618286),
        new google.maps.LatLng(25.375050, 55.799561),
        new google.maps.LatLng(25.362641, 55.794067),
        new google.maps.LatLng(25.345267, 55.813293),
        new google.maps.LatLng(25.322925, 55.794067),
        new google.maps.LatLng(25.327890, 55.913544),
        new google.maps.LatLng(25.355196, 55.912170),
        new google.maps.LatLng(25.387457, 55.951996),
        new google.maps.LatLng(25.397382, 55.993195),
        new google.maps.LatLng(25.389939, 56.012421),
        new google.maps.LatLng(25.341544, 55.994568),
        new google.maps.LatLng(25.337820, 55.971222),
        new google.maps.LatLng(25.322925, 55.965729),
        new google.maps.LatLng(25.316718, 55.973969),
        new google.maps.LatLng(25.305545, 55.978088),
        new google.maps.LatLng(25.291888, 55.991821),
        new google.maps.LatLng(25.264568, 56.000061),
        new google.maps.LatLng(25.227305, 56.001434),
        new google.maps.LatLng(25.193758, 55.983582),
        new google.maps.LatLng(25.158958, 56.002808),
        new google.maps.LatLng(25.086843, 55.997314),
        new google.maps.LatLng(25.034594, 55.979462),
        new google.maps.LatLng(25.000994, 55.979462),
        new google.maps.LatLng(25.003484, 55.962982),
        new google.maps.LatLng(24.963650, 55.914917),
        new google.maps.LatLng(24.964895, 55.854492),
        new google.maps.LatLng(24.908858, 55.814667),
        new google.maps.LatLng(24.801695, 55.814667)
    ];

    var ajmanCoordinates = [
        new google.maps.LatLng(25.465594, 55.441132),
        new google.maps.LatLng(25.438314, 55.486450),
        new google.maps.LatLng(25.444515, 55.537262),
        new google.maps.LatLng(25.419710, 55.622406),
        new google.maps.LatLng(25.408547, 55.630646),
        new google.maps.LatLng(25.367605, 55.599060),
        new google.maps.LatLng(25.362641, 55.555115),
        new google.maps.LatLng(25.404825, 55.393066)
    ];

    var ummAlKhaimanCoordinates = [
        new google.maps.LatLng(25.546159, 55.464478),
        new google.maps.LatLng(25.429633, 55.623779),
        new google.maps.LatLng(25.373809, 55.800934),
        new google.maps.LatLng(25.365123, 55.791321),
        new google.maps.LatLng(25.345267, 55.810547),
        new google.maps.LatLng(25.322925, 55.789948),
        new google.maps.LatLng(25.324167, 55.917664),
        new google.maps.LatLng(25.356437, 55.913544),
        new google.maps.LatLng(25.383735, 55.956116),
        new google.maps.LatLng(25.409787, 55.916290),
        new google.maps.LatLng(25.439554, 55.920410),
        new google.maps.LatLng(25.528811, 55.818787),
        new google.maps.LatLng(25.611810, 55.827026),
        new google.maps.LatLng(25.624192, 55.816040),
        new google.maps.LatLng(25.627907, 55.795441),
        new google.maps.LatLng(25.710837, 55.721283)
    ];

    var rasAlKhaimanCoordinates = [
        new google.maps.LatLng(25.739292, 55.677338),
        new google.maps.LatLng(25.661333, 55.756989),
        new google.maps.LatLng(25.626669, 55.791321),
        new google.maps.LatLng(25.622954, 55.811920),
        new google.maps.LatLng(25.609333, 55.824280),
        new google.maps.LatLng(25.530050, 55.817413),
        new google.maps.LatLng(25.522615, 55.825653),
        new google.maps.LatLng(25.438314, 55.916290),
        new google.maps.LatLng(25.409787, 55.914917),
        new google.maps.LatLng(25.381254, 55.951996),
        new google.maps.LatLng(25.394901, 55.991821),
        new google.maps.LatLng(25.387457, 56.005554),
        new google.maps.LatLng(25.388698, 56.042633),
        new google.maps.LatLng(25.357678, 56.093445),
        new google.maps.LatLng(25.376291, 56.147003),
        new google.maps.LatLng(25.393661, 56.157990),
        new google.maps.LatLng(25.423431, 56.151123),
        new google.maps.LatLng(25.424672, 56.145630),
        new google.maps.LatLng(25.415989, 56.104431),
        new google.maps.LatLng(25.425912, 56.093445),
        new google.maps.LatLng(25.475513, 56.122284),
        new google.maps.LatLng(25.500306, 56.116791),
        new google.maps.LatLng(25.505263, 56.112671),
        new google.maps.LatLng(25.512700, 56.065979),
        new google.maps.LatLng(25.504024, 56.024780),
        new google.maps.LatLng(25.510221, 55.990448),
        new google.maps.LatLng(25.538724, 55.972595),
        new google.maps.LatLng(25.583324, 55.965729),
        new google.maps.LatLng(25.608094, 55.982208),
        new google.maps.LatLng(25.614286, 56.028900),
        new google.maps.LatLng(25.644002, 56.063232),
        new google.maps.LatLng(25.641526, 56.100311),
        new google.maps.LatLng(25.666285, 56.155243),
        new google.maps.LatLng(25.683613, 56.153870),
        new google.maps.LatLng(25.723210, 56.168976),
        new google.maps.LatLng(25.752899, 56.163483),
        new google.maps.LatLng(25.759083, 56.157990),
        new google.maps.LatLng(25.797418, 56.159363),
        new google.maps.LatLng(25.811018, 56.142883),
        new google.maps.LatLng(25.825853, 56.144257),
        new google.maps.LatLng(25.833269, 56.148376),
        new google.maps.LatLng(25.839449, 56.155243),
        new google.maps.LatLng(25.846865, 56.156616),
        new google.maps.LatLng(25.855516, 56.166229),
        new google.maps.LatLng(25.865402, 56.167603),
        new google.maps.LatLng(25.874052, 56.164856),
        new google.maps.LatLng(25.883937, 56.171722),
        new google.maps.LatLng(25.888879, 56.181335),
        new google.maps.LatLng(25.906174, 56.185455),
        new google.maps.LatLng(25.920996, 56.181335),
        new google.maps.LatLng(25.943227, 56.170349),
        new google.maps.LatLng(25.950636, 56.186829),
        new google.maps.LatLng(25.977799, 56.200562),
        new google.maps.LatLng(25.992612, 56.186829),
        new google.maps.LatLng(26.019766, 56.185455),
        new google.maps.LatLng(26.059250, 56.164856),
        new google.maps.LatLng(26.070353, 56.157990),
        new google.maps.LatLng(26.070353, 56.141510),
        new google.maps.LatLng(26.060484, 56.133270),
        new google.maps.LatLng(26.058017, 56.122284),
        new google.maps.LatLng(26.053082, 56.118164),
        new google.maps.LatLng(26.059250, 56.107178),
        new google.maps.LatLng(26.054315, 56.096191),
        new google.maps.LatLng(26.055549, 56.049500),
        new google.maps.LatLng(25.739292, 55.677338)
    ];

    var rasAlKhaimanSouthCoordinates = [
        new google.maps.LatLng(25.270778, 56.239014),
        new google.maps.LatLng(25.345267, 56.196442),
        new google.maps.LatLng(25.361400, 56.163483),
        new google.maps.LatLng(25.352714, 56.136017),
        new google.maps.LatLng(25.300579, 56.136017),
        new google.maps.LatLng(25.291888, 56.155243),
        new google.maps.LatLng(25.283196, 56.116791),
        new google.maps.LatLng(25.243454, 56.116791),
        new google.maps.LatLng(25.229789, 56.094818),
        new google.maps.LatLng(25.247180, 56.065979),
        new google.maps.LatLng(25.231032, 56.037140),
        new google.maps.LatLng(25.229789, 56.000061),
        new google.maps.LatLng(25.195000, 55.978088),
        new google.maps.LatLng(25.160201, 55.997314),
        new google.maps.LatLng(25.084355, 55.990448),
        new google.maps.LatLng(25.029617, 55.972595),
        new google.maps.LatLng(25.003484, 55.972595),
        new google.maps.LatLng(24.998505, 55.976715),
        new google.maps.LatLng(24.992282, 56.004181),
        new google.maps.LatLng(24.969875, 56.027527),
        new google.maps.LatLng(24.964895, 56.042633),
        new google.maps.LatLng(24.944974, 56.038513),
        new google.maps.LatLng(24.944974, 56.052246),
        new google.maps.LatLng(24.907613, 56.037140),
        new google.maps.LatLng(24.885191, 56.038513),
        new google.maps.LatLng(24.877716, 56.046753),
        new google.maps.LatLng(24.870240, 56.045380),
        new google.maps.LatLng(24.862765, 56.049500),
        new google.maps.LatLng(24.837842, 56.098938),
        new google.maps.LatLng(24.834103, 56.125031),
        new google.maps.LatLng(24.831610, 56.204681),
        new google.maps.LatLng(24.845319, 56.208801),
        new google.maps.LatLng(24.856534, 56.260986),
        new google.maps.LatLng(24.871486, 56.281586),
        new google.maps.LatLng(24.880207, 56.284332),
        new google.maps.LatLng(24.887682, 56.284332),
        new google.maps.LatLng(24.931276, 56.186829),
        new google.maps.LatLng(24.986058, 56.185455),
        new google.maps.LatLng(25.060721, 56.142883),
        new google.maps.LatLng(25.227305, 56.160736),
        new google.maps.LatLng(25.242212, 56.210175),
        new google.maps.LatLng(25.252149, 56.212921),
        new google.maps.LatLng(25.265810, 56.233521),
        new google.maps.LatLng(25.270778, 56.239014)
    ]

    var fujairahCoordinates = [
        new google.maps.LatLng(25.648954, 56.060486),
        new google.maps.LatLng(25.645240, 56.097565),
        new google.maps.LatLng(25.673711, 56.156616),
        new google.maps.LatLng(25.619239, 56.177216),
        new google.maps.LatLng(25.618001, 56.200562),
        new google.maps.LatLng(25.613048, 56.203308),
        new google.maps.LatLng(25.611810, 56.236267),
        new google.maps.LatLng(25.604379, 56.252747),
        new google.maps.LatLng(25.605618, 56.262360),
        new google.maps.LatLng(25.611810, 56.251373),
        new google.maps.LatLng(25.616763, 56.250000),
        new google.maps.LatLng(25.630383, 56.260986),
        new google.maps.LatLng(25.631622, 56.269226),
        new google.maps.LatLng(25.636574, 56.329651),
        new google.maps.LatLng(25.625431, 56.390076),
        new google.maps.LatLng(25.373809, 56.410675),
        new google.maps.LatLng(25.373809, 56.365356),
        new google.maps.LatLng(25.373809, 56.335144),
        new google.maps.LatLng(25.361400, 56.333771),
        new google.maps.LatLng(25.335338, 56.324158),
        new google.maps.LatLng(25.301821, 56.317291),
        new google.maps.LatLng(25.308028, 56.309052),
        new google.maps.LatLng(25.304304, 56.303558),
        new google.maps.LatLng(25.314236, 56.300812),
        new google.maps.LatLng(25.312994, 56.292572),
        new google.maps.LatLng(25.311753, 56.284332),
        new google.maps.LatLng(25.324167, 56.269226),
        new google.maps.LatLng(25.325408, 56.263733),
        new google.maps.LatLng(25.314236, 56.243134),
        new google.maps.LatLng(25.309270, 56.237640),
        new google.maps.LatLng(25.303062, 56.243134),
        new google.maps.LatLng(25.295613, 56.248627),
        new google.maps.LatLng(25.276987, 56.248627),
        new google.maps.LatLng(25.269536, 56.247253),
        new google.maps.LatLng(25.263327, 56.230774),
        new google.maps.LatLng(25.340303, 56.192322),
        new google.maps.LatLng(25.355196, 56.166229),
        new google.maps.LatLng(25.348990, 56.140137),
        new google.maps.LatLng(25.322925, 56.142883),
        new google.maps.LatLng(25.316718, 56.164856),
        new google.maps.LatLng(25.293130, 56.164856),
        new google.maps.LatLng(25.288163, 56.162109),
        new google.maps.LatLng(25.278229, 56.125031),
        new google.maps.LatLng(25.240969, 56.123657),
        new google.maps.LatLng(25.222335, 56.096191),
        new google.maps.LatLng(25.239727, 56.067352),
        new google.maps.LatLng(25.224820, 56.038513),
        new google.maps.LatLng(25.226063, 55.997314),
        new google.maps.LatLng(25.259601, 55.997314),
        new google.maps.LatLng(25.288163, 55.986328),
        new google.maps.LatLng(25.295613, 55.993195),
        new google.maps.LatLng(25.315477, 56.008301),
        new google.maps.LatLng(25.327890, 56.027527),
        new google.maps.LatLng(25.334097, 56.031647),
        new google.maps.LatLng(25.337820, 56.028900),
        new google.maps.LatLng(25.339061, 55.993195),
        new google.maps.LatLng(25.345267, 55.990448),
        new google.maps.LatLng(25.394901, 56.005554),
        new google.maps.LatLng(25.394901, 56.044006),
        new google.maps.LatLng(25.363882, 56.093445),
        new google.maps.LatLng(25.380013, 56.141510),
        new google.maps.LatLng(25.396142, 56.151123),
        new google.maps.LatLng(25.419710, 56.147003),
        new google.maps.LatLng(25.409787, 56.101685),
        new google.maps.LatLng(25.424672, 56.085205),
        new google.maps.LatLng(25.475513, 56.115417),
        new google.maps.LatLng(25.500306, 56.107178),
        new google.maps.LatLng(25.506503, 56.065979),
        new google.maps.LatLng(25.497827, 56.023407),
        new google.maps.LatLng(25.504024, 55.984955),
        new google.maps.LatLng(25.537485, 55.964355),
        new google.maps.LatLng(25.583324, 55.957489),
        new google.maps.LatLng(25.614286, 55.978088),
        new google.maps.LatLng(25.622954, 56.026154),
        new google.maps.LatLng(25.648954, 56.060486)
    ];

    var fujairahSouthCoordinates = [
        new google.maps.LatLng(25.376291, 56.359863),
        new google.maps.LatLng(25.376291, 56.332397),
        new google.maps.LatLng(25.305545, 56.304932),
        new google.maps.LatLng(25.291888, 56.317291),
        new google.maps.LatLng(25.290646, 56.346130),
        new google.maps.LatLng(25.263327, 56.344757),
        new google.maps.LatLng(25.257117, 56.325531),
        new google.maps.LatLng(25.238485, 56.280212),
        new google.maps.LatLng(25.234758, 56.262360),
        new google.maps.LatLng(25.239727, 56.223907),
        new google.maps.LatLng(25.244696, 56.210175),
        new google.maps.LatLng(25.229789, 56.159363),
        new google.maps.LatLng(25.061965, 56.138763),
        new google.maps.LatLng(24.979834, 56.182709),
        new google.maps.LatLng(24.931276, 56.184082),
        new google.maps.LatLng(24.931276, 56.271973),
        new google.maps.LatLng(24.907613, 56.295319),
        new google.maps.LatLng(24.920068, 56.325531),
        new google.maps.LatLng(25.093061, 56.328278),
        new google.maps.LatLng(25.096792, 56.376343),
        new google.maps.LatLng(25.098036, 56.436768),
        new google.maps.LatLng(25.376291, 56.455994)
    ]

    var centralAjmanCoordinates = [
        new google.maps.LatLng(25.339061, 55.965729),
        new google.maps.LatLng(25.344026, 56.019287),
        new google.maps.LatLng(25.331614, 56.039886),
        new google.maps.LatLng(25.288163, 55.991821),
        new google.maps.LatLng(25.300579, 55.978088),
        new google.maps.LatLng(25.316718, 55.971222),
        new google.maps.LatLng(25.320443, 55.962982),
        new google.maps.LatLng(25.339061, 55.965729)
    ];

    // Borders
    // Ajman
    this.ajmanBorders = new google.maps.Polygon({
        path: ajmanCoordinates
    });
    this.centralAjmanBorders = new google.maps.Polygon({
        path: centralAjmanCoordinates
    });

    // Dubai
    this.dubaiBorders = new google.maps.Polygon({
        path: dubaiCoordinates
    });
    this.dubaiEastBorders = new google.maps.Polygon({
        path: dubaiEastCoordinates
    });

    // Abu Dhabi
    this.abuDhabiBorders = new google.maps.Polygon({
        path: abuDhabiCoordinates
    });

    // Sharjah
    this.sharjahBorders = new google.maps.Polygon({
        path: sharjahCoordinates
    });
    this.sharjahEastBorders = new google.maps.Polygon({
        path: sharjahEastCoordinates
    });

    // Umm Al Khaiman
    this.ummAlKhaimanBorders = new google.maps.Polygon({
        path: ummAlKhaimanCoordinates
    });

    // Ras al Khaiman
    this.rasAlKhaimanBorders = new google.maps.Polygon({
        path: rasAlKhaimanCoordinates
    });
    this.rasAlKhaimanSouthBorders = new google.maps.Polygon({
        path: rasAlKhaimanSouthCoordinates
    });

    // Fujairah
    this.fujairaBorders = new google.maps.Polygon({
        path: fujairahCoordinates
    });
    this.fujairaSouthBorders = new google.maps.Polygon({
        path: fujairahSouthCoordinates
    });
};
