patternWindow.prototype.uaePattern4 = function() {
    var width = this.windowWidth;

    // Update Tween
    TWEEN.update();

    // Base variables
    var longitude = this.params.longitude;
    var longitudeMin = this.params.longitudeMin;
    var longitudeS = this.params.longitudeS;
    var latitude = this.params.latitude;
    var latitudeMin = this.params.latitudeMin;
    var latitudeS = this.params.latitudeS;

    // Set canvas style
    this.setStyle();

    this.windowContext.clearRect(0, 0, width, 900);

    // Fixed values
    var spaceDifference = width / 10;
    var lineNumber = 15;
    var circlesNumber = 20;

    // Changeable vars
    var spaceDifferenceDeviation = map(longitude, 51, 57, 0, width / 1000);
    var initialMarginVariation = map(latitude, 22, 27, -width / 50, width / 50);
    var oddLinesDeviation = map(longitudeMin, 0, 60, 0, 60);
    var lineRotationIncrease = map(latitudeMin, 0, 60, 0, 30);
    var evenLinesDeviation = map(latitudeMin, 0, 60, -60, 0);
    var rotationIncrease = map(longitudeS, 0, 60, 0, 8);
    var baseRotationIncrease = map(latitudeS, 0, 60, 0, 32);

    this.windowContext.beginPath();
    for (s = 0; s < circlesNumber; s++) {
        var point1 = [-width * 3, 0];
        var point2 = [width * 3, 0];

        var baseRotation = 18 + rotationIncrease;
        var initialMargin = width / 20 + initialMarginVariation;

        if (s > circlesNumber / 2) { // After the tenth circle, the rotation changes
            baseRotation += baseRotationIncrease;
        }

        if (s < circlesNumber / 4) {
            baseRotation -= baseRotationIncrease;
        }

        if (s > circlesNumber / 2) {
            initialMargin += 50;
        }

        // Rotation 1
        var rotationAngle1 = toRad(baseRotation + (s * 108));
        var hipotenusa1 = point2[0];
        var catetoOposto1 = Math.sin(rotationAngle1) * hipotenusa1;
        var catetoAdjacente1 = Math.cos(rotationAngle1) * hipotenusa1;

        point1[0] = catetoAdjacente1;
        point1[1] = catetoOposto1;
        point2[0] = -catetoAdjacente1;
        point2[1] = -catetoOposto1;

        // Translation 1
        var rotationAngle2 = toRad(270 + (baseRotation + (s * 108)));
        var hipotenusa2 = -initialMargin;
        var translationX = Math.cos(rotationAngle2) * hipotenusa2;
        var translationY = Math.sin(rotationAngle2) * hipotenusa2;

        point1[0] += translationX;
        point1[1] += translationY;
        point2[0] += translationX;
        point2[1] += translationY;

        // Screen middle
        point1[1] += this.windowHeight / 2;
        point2[1] += this.windowHeight / 2;

        for (var i = 0; i < lineNumber; i++) {
            var linesTranslationY = spaceDifference + i * 15;
            var rotationAngle3;

            if (i > lineNumber / 5) {
                rotationAngle3 = toRad(lineRotationIncrease + 270 + (baseRotation + (s * 108)));
            } else {
                rotationAngle3 = toRad(270 + (baseRotation + (s * 108)));
            }

            if ((i % 2) != 0) {
                rotationAngle3 += toRad(oddLinesDeviation);
                spaceDifference += spaceDifferenceDeviation;
            } else {
                rotationAngle3 += toRad(evenLinesDeviation);
            }

            // Translation 2
            var hipotenusa3 = -linesTranslationY;
            var translationX2 = Math.cos(rotationAngle3) * hipotenusa3;
            var translationY2 = Math.sin(rotationAngle3) * hipotenusa3;

            point1[0] += translationX2;
            point1[1] += translationY2;
            point2[0] += translationX2;
            point2[1] += translationY2;

            this.windowContext.moveTo(point1[0], point1[1]);
            this.windowContext.lineTo(point2[0], point2[1]);
        };
    };
    this.windowContext.stroke();

    // If it needs another update, call the function again
    if (this.updating) {
        requestAnimationFrame(this.uaePattern4.bind(this));
    }
}
