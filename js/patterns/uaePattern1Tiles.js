// Losango menor
patternWindow.prototype.defineTiles = function() {
    var params = this.params;
    var m = this.sizeMultiplier;

    // Simple influences
    var influence1 = map(params.longitudeMin, 0, 60, -5, 4);
    var influence2 = map(params.latitudeS, 0, 60, -4, 6);
    var influence3 = map(params.latitude, 22, 27, -1, 7);
    var influence4 = map(params.longitude, 51, 57, -7, 8);
    var influence6 = map(params.longitudeS, 0, 60, -2, 2);

    // Complex ones
    var influence7Y = map(params.longitude, 51, 57, -4, 11) * Math.sin(toRad(54));
    var influence7X = map(params.longitude, 51, 57, -4, 11) * Math.cos(toRad(54));
    var influence8Y = map(params.latitudeS, 0, 60, -7, 9) * Math.sin(toRad(108));
    var influence8X = map(params.latitudeS, 0, 60, -7, 9) * Math.cos(toRad(108));
    var influence5Y = map(params.longitudeMin, 0, 60, -3, 7) * Math.sin(toRad(72));
    var influence5X = map(params.longitudeMin, 0, 60, -3, 7) * Math.cos(toRad(72));
    var influence9Y = map(params.latitudeMin, 0, 60, -4, 4) * Math.sin(toRad(54));
    var influence9X = map(params.latitudeMin, 0, 60, -4, 4) * Math.cos(toRad(54));

    // Losango menor
    var uaeTile1 = {
        outerPoints: [
            [m * 50, m * 25],
            [m * 68.164, m * 50],
            [m * 50, m * 75],
            [m * 31.836, m * 50]
        ],
        innerPoints: [
            [m * (40.918 - influence9X), m * (37.5 + influence9Y)],
            [m * (59.082 + influence9X), m * (37.5 + influence9Y)],
            [m * (54.995 + influence1), m * (50.078)],
            [m * (59.047 + influence9X), m * (62.548 - influence9Y)],
            [m * (40.883 - influence9X), m * (62.548 - influence9Y)],
            [m * (44.97 - influence1), m * (49.97)]
        ]
    };

    // Losango
    var uaeTile2 = {
        outerPoints: [
            [m * 50, m * 9.549],
            [m * 68.164, m * 34.549],
            [m * 68.163, m * 65.451],
            [m * 50, m * 90.451],
            [m * 31.836, m * 65.451],
            [m * 31.837, m * 34.549]
        ],
        innerPoints: [
            [m * (40.918 - influence7X), m * (22.049 + influence7Y)],
            [m * (59.082 + influence7X), m * (22.049 + influence7Y)],
            [m * (53.469 + influence3), m * (39.324 + influence2)],
            [m * (68.164), m * (50)],
            [m * (53.469 + influence3), m * (60.676 - influence2)],
            [m * (59.082 + influence7X), m * (77.951 - influence7Y)],
            [m * (40.918 - influence7X), m * (77.951 - influence7Y)],
            [m * (46.531 - influence3), m * (60.676 - influence2)],
            [m * (31.836), m * (50)],
            [m * (46.531 - influence3), m * (39.324 + influence2)]
        ]
    };

    // Pentagono
    var uaeTile3 = {
        outerPoints: [
            [m * 50, m * 26.224],
            [m * 75, m * 44.387],
            [m * 65.451, m * 73.776],
            [m * 34.549, m * 73.776],
            [m * 25, m * 44.387]
        ],
        innerPoints: [
            [m * 37.5, m * 35.305],
            [m * 49.95, m * 39.383],
            [m * 62.5, m * 35.305],
            [m * 62.5, m * 48.449],
            [m * 70.225, m * 59.082],
            [m * 57.675, m * 63.16],
            [m * 50, m * 73.776],
            [m * 42.325, m * 63.16],
            [m * 29.775, m * 59.082],
            [m * 37.531, m * 48.406]
        ]
    };

    // Gravatinha
    var uaeTile4 = {
        outerPoints: [
            [m * 34.549, m * 20.61],
            [m * 65.451, m * 20.61],
            [m * 55.902, m * 50],
            [m * 65.451, m * 79.39],
            [m * 34.549, m * 79.39],
            [m * 44.098, m * 50]
        ],
        innerPoints: [
            [m * 50, m * 20.61],
            [m * (60.676 + influence8X), m * (35.305 + influence8Y)],
            [m * 50, m * (38.774 + influence4)],
            [m * (39.324 - influence8X), m * (35.305 + influence8Y)]
        ],
        innerPoints2: [
            [m * 50, m * (61.226 - influence4)],
            [m * (60.676 + influence8X), m * (64.695 - influence8Y)],
            [m * 50, m * 79.39],
            [m * (39.324 - influence8X), m * (64.695 - influence8Y)]
        ]
    };

    // Decagono
    var uaeTile5 = {
        outerPoints: [
            [m * 34.349, m * 2.447],
            [m * 65.451, m * 2.447],
            [m * 90.451, m * 20.611],
            [m * 100, m * 50],
            [m * 90.451, m * 79.389],
            [m * 65.451, m * 97.553],
            [m * 34.549, m * 97.553],
            [m * 9.549, m * 79.389],
            [m * 0, m * 50],
            [m * 9.549, m * 20.611],
        ],
        innerPoints: [
            [m * 50, m * 2.447],
            [m * 95.225, m * 64.695],
            [m * 22.049, m * 88.471],
            [m * 22.049, m * 11.529],
            [m * 95.225, m * 35.305],
            [m * 50, m * 97.553],
            [m * 4.775, m * 35.305],
            [m * 77.951, m * 11.529],
            [m * 77.951, m * 88.471],
            [m * 4.775, m * 64.695]
        ]
    };

    // Penrose
    var uaeTile6 = {
        outerPoints: [
            [m * 50, m * 20.611],
            [m * 59.549, m * 50],
            [m * 50, m * 79.389],
            [m * 40.451, m * (50)]
        ],
        innerPoints: [
            [m * (45.225 - influence5X), m * (35.305 + influence5Y)],
            [m * (54.775 + influence5X), m * (35.305 + influence5Y)],
            [m * (51.917 + influence6), m * (50)],
            [m * (54.775 + influence5X), m * (64.695 - influence5Y)],
            [m * (45.225 - influence5X), m * (64.695 - influence5Y)],
            [m * (48.083 - influence6), m * (50)],
        ]
    };

    // Master tile
    this.masterTile = {
        master: true,
        tiles: [
            [uaeTile6, this.radiusUnit * 0.17275, this.radiusUnit * 0.73776, -36],
            [uaeTile6, this.radiusUnit * 0.625, this.radiusUnit * 0.88471, 72],
            [uaeTile6, this.radiusUnit * 0.59549, this.radiusUnit * 0.5, 0],
            [uaeTile6, this.radiusUnit * 0.42275, this.radiusUnit * 0.44387, 36],
            [uaeTile6, this.radiusUnit * 0.625, this.radiusUnit * 0.11529, -72],
            [uaeTile1, this.radiusUnit * 0.17275, this.radiusUnit * 0.44387, -18],
            [uaeTile1, this.radiusUnit * 0.34549, this.radiusUnit * 0.20611, 90],
            [uaeTile1, this.radiusUnit * 0.79775, this.radiusUnit * 0.35334, -54],
            [uaeTile1, this.radiusUnit * 0.79775, this.radiusUnit * 0.64695, 54],
            [uaeTile1, this.radiusUnit * 0.42275, this.radiusUnit * 0.73776, 18]
        ]
    };

    // Pattern
    this.pattern = [
        // [tile, tileRotation, initialRotation, placementInterval, radius]
        [uaeTile2, 90, 18, 36, this.radiusUnit * 0.9045142857],
        [uaeTile2, 0, 0, 36, this.radiusUnit * 1.2449657143],
        [uaeTile2, 0, 0, 36, this.radiusUnit * 2.1960628571],
        [uaeTile2, -26.98, -9.02, 36, this.radiusUnit * 2.4642914286],
        [uaeTile4, 90, 0, 36, this.radiusUnit * 0.77],
        [uaeTile4, 90, 0, 36, this.radiusUnit * 1.7205114286],
        [uaeTile4, 90, 0, 36, this.radiusUnit * 2.6715971429],
        [uaeTile4, -9.94, -8.06, 36, this.radiusUnit * 2.7549457143],
        [uaeTile5, 18, 0, 360, this.radiusUnit * 0],
        [this.masterTile, 72, -18, 36, this.radiusUnit * 1.8090342857],
        [this.masterTile, 6.16, -24.18, 36, this.radiusUnit * 2.7276657143]
    ];
};
