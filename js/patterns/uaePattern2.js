patternWindow.prototype.uaePattern2 = function() {
    var width = this.windowWidth;

    // Update Tween
    TWEEN.update();

    // Base variables
    var longitude = this.params.longitude;
    var longitudeMin = this.params.longitudeMin;
    var longitudeS = this.params.longitudeS;
    var latitude = this.params.latitude;
    var latitudeMin = this.params.latitudeMin;
    var latitudeS = this.params.latitudeS;

    this.windowContext.save();

    // Set canvas style
    this.setStyle();

    this.windowContext.clearRect(0, 0, width, 900);

    var rotation1Deviation = map(latitude, 22, 27, -15, 15);
    var thirdStarSpace = map(latitude, 22, 27, 0, width / 20);
    var firstStarSpace = map(latitudeMin, 0, 60, 0, width / 10);
    var spaceDifferenceIncrease = map(latitudeS, 0, 60, width / 333.33333333, width / 166.66666667);
    var secondStarSpace = map(longitude, 51, 57, 0, width / 10);
    var rotation2Deviation = map(longitudeMin, 0, 60, 0, 0.35);
    var spaceDifference = map(longitudeS, 0, 60, width / 25, width / 8.33);
    var initialMargin = width / 40;
    var lineNumber = width / 66.666666667;

    // this.windowContext.lineWidth = 9;

    this.windowContext.beginPath();
    for (var i = 0; i < lineNumber * 10; i++) {
        var point1 = [-width * 3, 0];
        var point2 = [width * 3, 0];

        var translationY = spaceDifference + i * spaceDifferenceIncrease;

        if (i > 10) { // After first complete rotation
            translationY += firstStarSpace;
        }

        if (i > 20) { // After first complete rotation
            translationY += secondStarSpace;
        }

        if (i > 30) { // After first complete rotation
            translationY += thirdStarSpace;
        }

        // Rotation 1
        var catetoOposto1 = point2[0];
        var catetoAdjacente1 = point2[1];
        var hipotenusa1 = Math.sqrt((catetoOposto1 * catetoOposto1) + (catetoAdjacente1 * catetoAdjacente1));
        var rotationAngle1 = -toRad(18 + (i * 18) + (i * 9) + rotation1Deviation);

        point1[0] = -Math.sin(rotationAngle1) * hipotenusa1;
        point2[0] = Math.sin(rotationAngle1) * hipotenusa1;
        point1[1] = -Math.cos(rotationAngle1) * hipotenusa1;
        point2[1] = Math.cos(rotationAngle1) * hipotenusa1;

        // Translation 1
        var rotationAngle2 = toRad((18 + (i * 18) + (i * (9 + rotation2Deviation))));
        var hipotenusa2 = -initialMargin - translationY;
        var translationX = (Math.cos(rotationAngle2) * hipotenusa2);
        var translationY = Math.sin(rotationAngle2) * hipotenusa2;

        point1[0] += translationX;
        point1[1] += translationY;
        point2[0] += translationX;
        point2[1] += translationY;

        // Screen middle
        point1[1] += this.windowHeight / 2;
        point2[1] += this.windowHeight / 2;

        this.windowContext.moveTo(point1[0], point1[1]);
        this.windowContext.lineTo(point2[0], point2[1]);
    };
    this.windowContext.stroke();

    // If it needs another update, call the function again
    if (this.updating) {
        requestAnimationFrame(this.uaePattern2.bind(this));
    }
}
