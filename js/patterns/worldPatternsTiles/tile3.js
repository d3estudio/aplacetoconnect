patternWindow.prototype.tile3 = {
    tileWidth: 100,
    tileHeight: 100,
    thresholdRate: 0,
    horizontalTiles: 5, // Number of horizontal tiles
    verticalTiles: 5, // Number of vertical tiles
    lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS) {
        var lines = [
            [ // Fixed
                [0, 20], // P1
                [50, 0], // P2
                [100, 20], // P3
                [100, 80], // P4
                [50, 100], // P5
                [0, 80], // P6
                [0, 20], // P7
                [50, 40], // P8
                [100, 20] // P9
            ],
            [ // Fixed line
                [50, 40], // P10
                [50, 100] // P11
            ],
            [ // Big1
                [map(latitude, 0, 90, -10, 12) + 25, map(latitude, 0, 90, -3, 5) + 30], // P12
                [map(latitude, 0, 90, -10, 12) + 75, map(latitude, 0, 90, -3, 5) + 10] // P13
            ],
            [ // Big2
                [map(latitude, 0, 90, -12, 15) * -1 + 75, map(latitude, 0, 90, -5, 7) + 30], // P14
                [map(latitude, 0, 90, -12, 15) * -1 + 75, map(latitude, 0, 90, -5, 7) + 90], // P15
            ],
            [ // Big3
                [0, map(latitude, 0, 90, -12, 15) * -1 + 50], // P16
                [50, map(latitude, 0, 90, -12, 15) * -1 + 70] // P17
            ],
            [
                [map(latitudeMin, 0, 60, -10, 10.5) + map(latitude, 0, 90, -10, 12) + 48, map(latitudeMin, 0, 60, -4, 4.5) * -1 + map(latitude, 0, 90, -3, 5) + 20], // P18
                [map(latitudeS, 0, 60, -3, 4) + map(longitude, 0, 180, -15, 15) + 74, map(latitudeS, 0, 60, -1, 1) * -1 + map(longitude, 0, 180, -5, 5) * -1 + 30] // P19
            ],
            [
                [map(latitude, 0, 90, -12, 15) * -1 + 75, map(latitudeMin, 0, 60, -16.5, 13.5) + map(latitude, 0, 90, -3, 5) + 60], // P20
                [50, map(longitude, 0, 180, -15, 15) + map(latitudeS, 0, 60, -6, 6) + 70] // P21
            ],
            [
                [map(latitudeS, 0, 60, -5, 5) * -1 + map(longitude, 0, 180, -15, 15) * -1 + 25, map(latitudeS, 0, 60, -2, 2) * -1 + map(longitude, 0, 180, -7, 7) * -1 + 30], // P22
                [map(latitudeMin, 0, 60, -13.5, 10.5) * -1 + 25, map(latitudeMin, 0, 60, -5, 5) * -1 + map(latitude, 0, 90, -12, 15) * -1 + 60] // P23
            ],
            [
                [map(longitudeMin, 0, 60, -5, 4) + map(latitude, 0, 90, -10, 12) + 48, map(longitudeMin, 0, 60, -4, 2) * -1 + map(latitude, 0, 90, -3, 5) + 20], // P24
                [map(longitudeS, 0, 60, -11, 12) + 24, map(longitudeS, 0, 60, -5, 4) * -1 + 10] // P25
            ],
            [
                [map(latitude, 0, 90, -12, 15) * -1 + 75, map(longitudeMin, 0, 60, -8, 6) + map(latitude, 0, 90, -3, 5) + 60], // P26
                [100, map(longitudeS, 0, 60, -15, 15) + 50] // P27
            ],
            [
                [map(longitudeMin, 0, 60, -6, 5) * -1 + 25, map(longitudeMin, 0, 60, -2, 2) * -1 + map(latitude, 0, 90, -12, 15) * -1 + 60], // P28
                [map(longitudeS, 0, 60, -12, 12) * -1 + 25, map(longitudeS, 0, 60, -5, 5) * -1 + 90] // P29
            ]
        ];

        return lines;
    },
    replicator: function() {
        // Replicate the tile
        for (iy = 0; iy < this.tile.verticalTiles + 3; iy++) {
            for (ix = 0; ix < this.tile.horizontalTiles + 1; ix++) { // +1 for this module only
                // Saves the coordinate system
                this.windowContext.save();

                // Translation
                this.windowContext.translate(this.tile.tileWidth * ix - this.tile.tileWidth / 2, this.tile.tileHeight * iy - this.tile.tileHeight / 4);

                this.windowContext.translate(0, (-this.tile.tileHeight / 100 * 20) * iy);
                if (iy % 2 == 0) { // X e Y par
                    this.windowContext.translate(this.tile.tileWidth / 2, 0);
                }

                this.windowContext.drawImage(this.tileCanvas, 0, 0);

                // Restores the coordinate system
                this.windowContext.restore();
            }
        }
    }
}
