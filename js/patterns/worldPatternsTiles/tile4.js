patternWindow.prototype.tile4 = {
    tileWidth: 100,
    tileHeight: 90,
    thresholdRate: 0,
    horizontalTiles: 5, // Number of horizontal tiles
    verticalTiles: 5, // Number of vertical tiles
    lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS) {
        var influence1 = map(latitude, -90, 0, -3, 15),
            influence2 = map(longitudeS, 0, 60, -10, 23),
            influence3 = map(latitudeMin, 0, 60, -15, 15),
            influence4 = map(latitudeS, 0, 60, -6, 6),
            lines = [
                [
                    [0, map(longitude, -180, 0, -10, 0) + 25], // P1
                    [influence4 + map(longitudeMin, 0, 60, -10, 0) + 25, influence4 + map(longitude, -180, 0, -10, 0) + 25], // P2
                    [map(longitudeMin, 0, 60, -10, 0) + 25, 0], // P3
                ],
                [
                    [map(longitudeMin, 0, 60, -10, 0) * -1 + 75, 100], // P4
                    [-influence4 + map(longitudeMin, 0, 60, -10, 0) * -1 + 75, -influence4 + map(longitude, -180, 0, -10, 0) * -1 + 75], // P5
                    [100, map(longitude, -180, 0, -10, 0) * -1 + 75], // P6
                ],
                [
                    [0, 100], // P7
                    [influence3 + 25, -influence3 + 75] // P8
                ],
                [
                    [100, 0], // P9
                    [-influence3 + 75, influence3 + 25] // P10
                ],
                [
                    [0, influence2 + influence1 + 50], // P11
                    [25, influence1 + 50] // P12
                ],
                [
                    [75, -influence1 + 50], // P13
                    [100, -influence2 + -influence1 + 50] // P14
                ],
                [
                    [influence2 + influence1 + 50, 0], // P15
                    [influence1 + 50, 25] // P16
                ],
                [
                    [-influence1 + 50, 75], // P17
                    [-influence2 + -influence1 + 50, 100] // P18
                ],
                [
                    [influence1 + 50, 25], // P16
                    [-influence3 + 75, influence3 + 25], // P10
                    [75, -influence1 + 50], // P13
                    [-influence1 + 50, 75], // P17
                    [influence3 + 25, -influence3 + 75], // P8
                    [25, influence1 + 50], // P12
                    [influence1 + 50, 25] // P16
                ],
                [
                    [0, 0],
                    [100, 0],
                    [100, 100],
                    [0, 100],
                    [0, 0]
                ]
            ];

        return lines;
    },
    replicator: function() {
        for (iy = 0; iy < this.tile.verticalTiles; iy++) {
            for (ix = 0; ix < this.tile.horizontalTiles; ix++) {
                // Saves the coordinate system
                this.windowContext.save();

                if (iy % 2 > 0) { // check for odd or even vertical lines
                    this.windowContext.translate(0, this.tile.tileHeight * (iy + 1));
                    this.windowContext.scale(1, -1);
                } else {
                    this.windowContext.translate(0, this.tile.tileHeight * iy);
                }

                if (ix % 2 > 0) { // check for odd or even horizontal lines
                    this.windowContext.translate(this.tile.tileWidth * (ix + 1), 0);
                    this.windowContext.scale(-1, 1);
                } else {
                    this.windowContext.translate(this.tile.tileWidth * ix, 0);
                }

                // Pastes the tile in its position
                this.windowContext.drawImage(this.tileCanvas, 0, 0);

                // Restores the coordinate system
                this.windowContext.restore();
            }
        }
    }
}
