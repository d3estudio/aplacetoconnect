patternWindow.prototype.tile1 = {
    tileWidth: 50,
    tileHeight: 45,
    thresholdRate: 6,
    horizontalTiles: 10, // Number of horizontal tiles
    verticalTiles: 10, // Number of vertical tiles
    lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS) {
        var lines = [
            [
                [-50, map(latitude, -90, 90, -5, 0) + 20], // P1
                [map(longitudeS, 0, 60, 0, 20) + 60, map(latitude, -90, 90, -5, 0) + 20], // P2
                [map(longitudeS, 0, 60, 0, 20) + 60, map(latitudeS, 0, 60, 0, 30) + 60], // P3
                [40, map(latitudeS, 0, 60, -5, 25) + 60], // P4
                [40, map(latitudeS, 0, 60, -5, 25) + 40], // P5
                [map(longitudeMin, 0, 60, -5, 0) + 20, map(latitudeS, 0, 60, 0, 20) + 40], // P6
                [map(longitudeMin, 0, 60, -5, 0) + 20, 80], // P7
                [map(longitudeS, 0, 60, -50, 0) + 80, 80], // P8
                [map(longitudeS, 0, 60, -50, 0) + 80, 0] // P9
            ],
            [
                [map(longitude, -180, 180, -15, 5) + 40, 0], // P10
                [map(longitude, -180, 180, -15, 5) + 40, 20], // P11
                [map(longitude, -180, 180, -15, 5) + 40, 0] // P10
            ],
            [
                [map(longitudeS, 0, 60, -50, 0) + 80, map(latitudeMin, -180, 180, -40, 10) + 60], // P12
                [100, map(latitudeMin, -180, 180, -40, 10) + 60], // P13
                [map(longitudeS, 0, 60, -50, 0) + 80, map(latitudeMin, -180, 180, -40, 10) + 60] // P12
            ],
            [
                [40, 80], // P14
                [40, 100], // P15
                [40, 80] // P14
            ]
        ];

        return lines;
    },
    replicator: function() {
        // Replicate the tile
        for (iy = 0; iy < this.tile.verticalTiles; iy++) {
            for (ix = 0; ix < this.tile.horizontalTiles; ix++) {
                // Saves the coordinate system
                this.windowContext.save();

                if (iy % 2 > 0) { // check for odd or even vertical lines
                    // Transforms the coordinate system
                    this.windowContext.translate(0, this.windowCanvas.height + this.tile.tileHeight);
                    this.windowContext.scale(1, -1);
                }

                if (ix % 2 > 0) { // check for odd or even horizontal lines
                    // Transforms the coordinate system
                    this.windowContext.translate(this.windowCanvas.width + this.tile.tileWidth, 0);
                    this.windowContext.scale(-1, 1);
                }

                // Pastes the tile in its position
                this.windowContext.drawImage(this.tileCanvas, this.tile.tileWidth * ix, this.tile.tileHeight * iy);

                // Restores the coordinate system
                this.windowContext.restore();
            }
        }
    }
}
