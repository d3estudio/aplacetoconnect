patternWindow.prototype.tile2 = {
    tileWidth: 50,
    tileHeight: 50,
    thresholdRate: 0,
    horizontalTiles: 10, // Number of horizontal tiles
    verticalTiles: 9, // Number of vertical tiles
    lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS) {
        var mainPoint1 = map(longitude, -180, 0, -5, 15) + 20,
            mainPoint2 = map(latitude, 0, 90, -5, 15) + 60,
            lines = [
                [
                    [0, mainPoint1], // P1
                    [mainPoint1, mainPoint1], // P2
                    [mainPoint1, 0], // P3
                ],
                [
                    [200, map(latitudeMin, 00, 60, -2, 15) + 20], // P4
                    [100, map(latitudeMin, 00, 60, -2, 15) + 20], // P4
                    [map(latitudeS, 00, 60, -5, 15) + 60, 20], // P5
                    [map(latitudeS, 00, 60, -5, 15) + 60, 20], // P5
                    [map(latitudeS, 00, 60, -5, 15) + 60, mainPoint2] // P6
                ],
                [
                    [map(latitudeMin, 00, 60, -2, 15) + 20, 200], // P7
                    [map(latitudeMin, 00, 60, -2, 15) + 20, 100], // P7
                    [map(longitudeMin, -60, 60, -40, 30) + 25, mainPoint2], // P8
                    [map(latitudeS, 0, 60, -5, 15) + 60, mainPoint2], // P6
                    [100, 100] // P10
                ],
                [
                    [mainPoint1, mainPoint1], // P2
                    [mainPoint1, mainPoint1], // P2
                    [map(latitudeS, 00, 60, -5, 15) + 60, map(longitudeS, 0, 60, -20, 5) + map(latitude, -90, 90, -15, 0) + 60]
                ]
            ];

        return lines;
    },
    replicator: function() {
        // Replicate the tile
        for (iy = 0; iy < this.tile.verticalTiles; iy++) {
            for (ix = 0; ix < this.tile.horizontalTiles; ix++) {
                // Saves the coordinate system
                this.windowContext.save();

                // Translation
                if (ix % 2 == 0 && iy % 2 == 0) { // X e Y par
                    this.windowContext.translate(this.tile.tileWidth * ix, this.tile.tileHeight * iy);
                } else if (ix % 2 == 0 && iy % 2 != 0) {
                    this.windowContext.translate(this.tile.tileWidth * ix, this.tile.tileHeight * iy);
                    this.windowContext.rotate(270 * (Math.PI / 180))
                    this.windowContext.translate(-this.tile.tileWidth, 0);
                } else if (ix % 2 != 0 && iy % 2 == 0) {
                    this.windowContext.translate(this.tile.tileWidth * ix, this.tile.tileHeight * iy);
                    this.windowContext.rotate(90 * (Math.PI / 180))
                    this.windowContext.translate(0, -this.tile.tileHeight);
                } else if (ix % 2 != 0 && iy % 2 != 0) {
                    this.windowContext.translate(this.tile.tileWidth * ix, this.tile.tileHeight * iy);
                    this.windowContext.rotate(180 * (Math.PI / 180))
                    this.windowContext.translate(-this.tile.tileWidth, -this.tile.tileHeight);
                }

                this.windowContext.drawImage(this.tileCanvas, 0, 0);

                // Restores the coordinate system
                this.windowContext.restore();
            }
        }
    }
}
