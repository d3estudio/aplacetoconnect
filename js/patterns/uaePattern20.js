patternWindow.prototype.uaePattern20 = function() {
    var width = this.windowWidth;
    var widthRelation = 1000 / width;
    var _this = this;

    // Update Tween
    TWEEN.update();

    // Base variables
    var longitude = this.params.longitude;
    var longitudeMin = this.params.longitudeMin;
    var longitudeS = this.params.longitudeS;
    var latitude = this.params.latitude;
    var latitudeMin = this.params.latitudeMin;
    var latitudeS = this.params.latitudeS;

    // Influence variables
    var rotationChange = map(longitude, 51, 57, 0, 3);
    var spacesBetweenCircles = width / 10 + map(latitude, 22, 27, 0, width / 33.333333);
    var patternRotation = map(latitudeS, 0, 60, 0, 18);
    var initialMargin = map(longitudeS, 0, 60, width / 50, width / 20);
    var firstCircleRotation = map(latitudeS, 0, 60, -60, 60);
    var firstCircleVariation = map(longitudeMin, 0, 60, 0, width / 25);
    var circleVariation = map(latitudeMin, 0, 60, 0, width / 10);

    var lineDirections = {
        values: [
            [-1, 1],
            [1, 1],
            [2, 1],
            [3, 1],
            [4, 1],
            [5, 1],
            [6, 1]
        ],
        draw: function(circleSpace, pointSpace) {
            for (cs = 0; cs < circles.values.length; cs++) {
                if (cs == 0) {
                    continue;
                }

                for (p = 0; p < circles.values[cs].length; p++) {
                    var x = circles.values[cs][p].x;
                    var y = circles.values[cs][p].y;
                    var nextCircle;
                    var nextPoint;
                    var nextX;
                    var nextY;

                    if (circles.values[cs + circleSpace] == undefined) {
                        continue;
                    } else {
                        nextCircle = circles.values[cs + circleSpace];
                    }

                    if (nextCircle[p + pointSpace] == undefined) {
                        nextPoint = nextCircle[pointSpace - 1];
                    } else {
                        nextPoint = nextCircle[p + pointSpace];
                    }

                    nextX = nextPoint.x;
                    nextY = nextPoint.y;

                    _this.windowContext.moveTo(x, y);
                    _this.windowContext.lineTo(nextX, nextY);
                };
            };
        },
        drawInsideBorder: function() {
            _this.windowContext.moveTo(circles.values[0][0].x, circles.values[0][0].y);
            for (var i = 0; i < circles.values[0].length; i++) {
                _this.windowContext.lineTo(circles.values[0][i].x, circles.values[0][i].y);
            };
            _this.windowContext.lineTo(circles.values[0][0].x, circles.values[0][0].y);
        }
    }

    var circles = {
        values: [],
        circlesNumber: 20,
        definer: function() {
            for (i = 0; i < circles.circlesNumber; i++) {
                var circle = [];

                if (i % 2 != 0) {

                }

                for (p = 0; p < 10; p++) {
                    var rotationAngle;
                    var spacer;

                    if (i == 0) {
                        rotationAngle = toRad(((p * 36 - i * rotationChange) % 360) + patternRotation - firstCircleRotation);

                        if ((p % 2) == 0) {
                            spacer = (i * spacesBetweenCircles) + initialMargin + firstCircleVariation;
                        } else {
                            spacer = (i * spacesBetweenCircles) + initialMargin;
                        }
                    } else {
                        rotationAngle = toRad(((p * 36 - i * rotationChange) % 360) + patternRotation);

                        if ((p % 2) == 0) {
                            spacer = (i * spacesBetweenCircles) + initialMargin + circleVariation;
                        } else {
                            spacer = (i * spacesBetweenCircles) + initialMargin;
                        }
                    }

                    var pointX = Math.sin(rotationAngle) * spacer;
                    var pointY = Math.cos(rotationAngle) * spacer;
                    var point = {
                        x: pointX,
                        y: pointY + _this.windowHeight / 2
                    };
                    circle.push(point);
                };
                circles.values.push(circle);
            };
        }
    }

    // Prepare canvas
    this.windowContext.clearRect(0, 0, 1000, 900);
    this.windowContext.setTransform(1, 0, 0, 1, 0, 0);
    this.windowContext.save();

    // Set canvas style
    this.setStyle();
    this.windowContext.lineCap = "round";

    // Defines the all the points
    circles.definer();

    // Draw all the lines
    this.windowContext.beginPath();
    lineDirections.values.forEach(function(element, index, array) {
        lineDirections.draw(element[0], element[1]);
    });

    // Draw the first border
    lineDirections.drawInsideBorder();

    // Make all the lines appear
    this.windowContext.stroke();

    // If it needs another update, call the function again
    if (this.updating) {
        requestAnimationFrame(this.uaePattern20.bind(this));
    }
}
