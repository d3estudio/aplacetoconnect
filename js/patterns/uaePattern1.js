patternWindow.prototype.uaePattern1 = function() {
    // stats.begin(); // Begin monitoring

    // Clear window canvas
    this.windowContext.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.uaeIntermediateContext.clearRect(0, 0, this.windowWidth, this.windowHeight);

    // Update Tween
    TWEEN.update();

    // Update tiles
    this.defineTiles();

    // Draw pattern
    for (var i = 0; i < this.pattern.length; i++) {
        this.radialReplicator(this.pattern[i][0], this.pattern[i][1], this.pattern[i][2], this.pattern[i][3], this.pattern[i][4], this.pattern[i][5]);
    };

    // Pastes main frame to other
    this.windowContext.drawImage(this.uaeIntermedianteCanvas, 0, 0);

    // stats.end(); // End monitoring

    if (this.updating) {
        requestAnimationFrame(this.uaePattern1.bind(this));
    } // Call another frame

}

var fabricCanvas = window._canvas = new fabric.Canvas('svg');

patternWindow.prototype.masterTileBuilder = function(masterTile, masterRotation) {
    var masterCtx = this.uaeMasterTilesContext;
    var masterCanvas = this.uaeMasterTilesCanvas;

    masterCtx.clearRect(0, 0, masterCanvas.width, masterCanvas.height);

    if (masterRotation != 0) {
        masterCtx.translate(masterCanvas.width / 2, masterCanvas.height / 2);
        masterCtx.rotate(toRad(masterRotation));
        masterCtx.translate(-masterCanvas.width / 2, -masterCanvas.height / 2);
    }

    for (var i = 0; i < masterTile.tiles.length; i++) {
        this.drawTile(masterTile.tiles[i][0], masterTile.tiles[i][3]);
        masterCtx.drawImage(this.uaeTilesCanvas, 0, 0, masterCanvas.width, masterCanvas.height, masterTile.tiles[i][1] - masterCanvas.width / 2, masterTile.tiles[i][2] - masterCanvas.height / 2, masterCanvas.width, masterCanvas.height);
    };

    // Restore context
    masterCtx.setTransform(1, 0, 0, 1, 0, 0);
};

patternWindow.prototype.drawTile = function(tile, tileRotation) {
    var tileCtx = this.uaeTilesContext;

    // Clear tile canvas
    tileCtx.clearRect(0, 0, this.uaeTileWidth, this.uaeTileHeight);

    // Rotate tile
    if (tileRotation != 0) {
        // Prepare context for rotation
        tileCtx.translate(this.uaeTileWidth / 2, this.uaeTileHeight / 2);

        // Rotate
        tileCtx.rotate(toRad(tileRotation));

        // Center context
        tileCtx.translate(-this.uaeTileWidth / 2, -this.uaeTileHeight / 2);
    }

    // outerPoints
    tileCtx.beginPath();
    for (i = 0; i < tile.outerPoints.length; i++) {
        if (i === 0) {
            tileCtx.moveTo(tile.outerPoints[i][0], tile.outerPoints[i][1]);
        } else {
            tileCtx.lineTo(tile.outerPoints[i][0], tile.outerPoints[i][1]);
        }
    };
    tileCtx.closePath();

    // fabric
    for (i = 0; i < tile.outerPoints.length - 1; i++) {
        fabricCanvas.add(new fabric.Line([tile.outerPoints[i][0], tile.outerPoints[i][1], tile.outerPoints[i+1][0], tile.outerPoints[i+1][1]], {
            fill: 'red',
            stroke: 'red',
            strokeWidth: 2
        }));
    };

    // Inner points
    for (i = 0; i < tile.innerPoints.length; i++) {
        if (i === 0) {
            tileCtx.moveTo(tile.innerPoints[i][0], tile.innerPoints[i][1]);
        } else {
            tileCtx.lineTo(tile.innerPoints[i][0], tile.innerPoints[i][1]);
        }
    };
    tileCtx.closePath();

    // fabric
    for (i = 0; i < tile.innerPoints.length - 1; i++) {
        fabricCanvas.add(new fabric.Line([tile.innerPoints[i][0], tile.innerPoints[i][1], tile.innerPoints[i+1][0], tile.innerPoints[i+1][1]], {
            fill: 'red',
            stroke: 'red',
            strokeWidth: 2
        }));
    };

    // Inner points 2
    if (tile.innerPoints2 != undefined) {
        for (i = 0; i < tile.innerPoints2.length; i++) {
            if (i === 0) {
                tileCtx.moveTo(tile.innerPoints2[i][0], tile.innerPoints2[i][1]);
            } else {
                tileCtx.lineTo(tile.innerPoints2[i][0], tile.innerPoints2[i][1]);
            }
        };
        tileCtx.closePath();

        // fabric
        for (i = 0; i < tile.innerPoints2.length - 1; i++) {
            fabricCanvas.add(new fabric.Line([tile.innerPoints2[i][0], tile.innerPoints2[i][1], tile.innerPoints2[i+1][0], tile.innerPoints2[i+1][1]], {
                fill: 'red',
                stroke: 'red',
                strokeWidth: 2
            }));
        };
    }

    // Stroke every path
    tileCtx.stroke();

    // Restore context
    tileCtx.setTransform(1, 0, 0, 1, 0, 0);
};

//document.write(fabricCanvas.toSVG());

patternWindow.prototype.radialReplicator = function(tile, tileRotation, initialRotation, placementInterval, radius) {
    var initialRotation = toRad(initialRotation);
    var halfWindowHeight = this.windowHeight / 2;
    var rotationInfluence = toRad(map(this.params.latitudeS, 0, 60, 0, 36 / 2)) + toRad(120); // this last 120 degrees added to rotation is to hide the 3 stripes that are not created

    if (tile != this.oldTile || tileRotation != this.oldRotation) {
        if (tile.master != true) {
            this.drawTile(tile, tileRotation);
        } else {
            this.masterTileBuilder(tile, tileRotation);
        }
    }

    this.oldTile = tile;
    this.oldRotation = tileRotation;

    for (i = 3; i < 360 / placementInterval; i++) { // i starts in 3 to avoid the creation of at least 3 stripes
        var interCtx = this.uaeIntermediateContext;
        var iterationRotation = (i * toRad(placementInterval)) + initialRotation + rotationInfluence;
        var horizontalTranslation = Math.sin(iterationRotation) * halfWindowHeight; // Calculate translation correction
        var verticalTranslation = (Math.cos(iterationRotation) * halfWindowHeight) - halfWindowHeight; // Calculate translation correction

        // Apply rotation
        interCtx.rotate(iterationRotation);

        // Correct translation
        interCtx.translate(horizontalTranslation, verticalTranslation);

        // Detect master or simple
        if (tile.master != true) {
            var tileCanvas = this.uaeTilesCanvas;

            // Paste tile
            interCtx.drawImage(tileCanvas, 0, 0, tileCanvas.width, tileCanvas.height, radius - tileCanvas.width / 2, this.windowHeight / 2 - tileCanvas.height / 2, tileCanvas.width, tileCanvas.height);
        } else {
            var masterCanvas = this.uaeMasterTilesCanvas;

            // Paste master
            interCtx.drawImage(masterCanvas, 0, 0, masterCanvas.width, masterCanvas.height, radius - masterCanvas.width / 2, this.windowHeight / 2 - masterCanvas.height / 2, masterCanvas.width, masterCanvas.height);
        }

        // Restore context
        interCtx.setTransform(1, 0, 0, 1, 0, 0);
    };
}
