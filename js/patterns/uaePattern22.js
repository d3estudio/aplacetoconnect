patternWindow.prototype.uaePattern22 = function() {
    var width = this.windowWidth;
    var widthRelation = 1000 / width;
    var _this = this;

    // Update Tween
    TWEEN.update();

    var longitude = this.params.longitude;
    var longitudeMin = this.params.longitudeMin;
    var longitudeS = this.params.longitudeS;
    var latitude = this.params.latitude;
    var latitudeMin = this.params.latitudeMin;
    var latitudeS = this.params.latitudeS;

    // Storage vars
    var circlesBase = [];
    var circlesOut = [];
    var stripeNumber = 0;
    var deviation = [];

    // Construction vars
    var initialSpace = width / 10 + map(latitudeS, 0, 60, 0, width / 20);
    var divider = 36;
    var initialRotation = map(longitudeS, 0, 60, 0, 18);
    var deviation = {
        values: [], // Stores the values for the deviations
        definer: function defineDeviation(x, y) { // Define those values
            for (var d = 0; d < 360 / divider; d++) {
                var varMultiplierDistance;
                var varMultiplierX;
                var varMultiplierY;
                var varSelector = parseInt(map(d, 0, 360 / divider - 1, 0, 3));
                var mapMax = width / 66.666666667;
                var mapMin = -width / 66.666666667;

                // Selects a set of variables to use
                if (varSelector == 0) {
                    varMultiplierDistance = map(longitudeS, 0, 60, mapMin, mapMax);
                    varMultiplierX = map(longitudeMin, 0, 60, mapMin, mapMax);
                    varMultiplierY = map(latitudeMin, 0, 60, mapMin, mapMax);
                } else if (varSelector == 1) {
                    varMultiplierDistance = map(longitudeMin, 0, 60, mapMin, mapMax);
                    varMultiplierX = map(latitudeS, 0, 60, mapMin, mapMax);
                    varMultiplierY = map(latitudeS, 0, 60, mapMin, mapMax);
                } else if (varSelector == 2) {
                    varMultiplierDistance = map(latitudeS, 0, 60, mapMin, mapMax);
                    varMultiplierX = map(latitudeMin, 0, 60, mapMin, mapMax);
                    varMultiplierY = map(latitudeMin, 0, 60, mapMin, mapMax);
                } else if (varSelector == 3) {
                    varMultiplierDistance = map(latitudeMin, 0, 60, mapMin, mapMax);
                    varMultiplierX = map(latitudeS, 0, 60, mapMin, mapMax);
                    varMultiplierY = map(latitudeMin, 0, 60, mapMin, mapMax);
                }

                // After the selection, the variables are defined
                var deviationDistance = (stripeNumber / 5) * varMultiplierDistance;
                var deviationX = (stripeNumber / 5) * varMultiplierX;
                var deviationY = (stripeNumber / 5) * varMultiplierY;

                deviation.values[d] = {
                    distance: deviationDistance, // The deviation distance
                    x: deviationX, // The deviation on the x axis
                    y: deviationY // The deviation on the y axis
                };
            };
        }
    };
    var widths = {
        values: [],
        circles: 15 * 5, // 6 is the number of location variables
        definer: function() { // Defines the widths of the circles
            var varSelector = 0;
            var minWidth = width / 200;
            var mapMax = width / 33.333333333;
            var circleMultiplier = width / 2000;

            // Based on the circle number, a variable is selected to change its width
            for (i = 0; i < widths.circles; i++) {
                if (varSelector == 0) {
                    widths.values[i] = minWidth + map(longitude, 51, 57, 0, mapMax) + i * circleMultiplier;
                    varSelector++;
                    continue;
                } else if (varSelector == 1) {
                    widths.values[i] = minWidth + map(longitudeMin, 0, 60, 0, mapMax) + i * circleMultiplier;
                    varSelector++;
                    continue;
                } else if (varSelector == 2) {
                    widths.values[i] = minWidth + map(longitudeS, 0, 60, 0, mapMax) + i * circleMultiplier;
                    varSelector++;
                    continue;
                } else if (varSelector == 3) {
                    widths.values[i] = minWidth + map(latitude, 22, 27, 0, mapMax) + i * circleMultiplier;
                    varSelector++;
                    continue;
                } else if (varSelector == 4) {
                    widths.values[i] = minWidth + map(latitudeMin, 0, 60, 0, mapMax) + i * circleMultiplier;
                    varSelector++;
                    continue;
                } else if (varSelector == 5) {
                    widths.values[i] = minWidth + map(latitudeS, 0, 60, 0, mapMax) + i * circleMultiplier;
                    varSelector = 0;
                }
            };
        }
    };

    this.windowContext.clearRect(0, 0, width, 900);
    this.windowContext.setTransform(1, 0, 0, 1, 0, 0);
    this.windowContext.save();

    // Set canvas style
    this.setStyle();
    this.windowContext.lineCap = "round";

    deviation.definer();
    widths.definer();
    widths.values.forEach(makeStars);

    // Call the functions
    function makeStars(element, index, array) {
        createCircle(element);
        initialSpace += element;

        stripeNumber++;
    }

    // Distribute the points
    function createCircle(circleWidth) {
        var circleBase = [];
        var circleOut = [];
        var halfRotation = divider / 2;
        var rotationDirection = 3;
        var stripeRotation = (halfRotation * stripeNumber) + (map(longitude, 51, 57, -rotationDirection, rotationDirection) * stripeNumber / 2);


        for (i = 0; i < 360 / divider; i++) {
            var angleMultiplier = toRad(((i * divider) + stripeRotation + initialRotation) % 360); // Defines the angle to project the point
            var distanceToCenter = initialSpace + deviation.values[i].distance; // The distance to the center
            var pointX = Math.sin(angleMultiplier) * (distanceToCenter + deviation.values[i].x); // X
            var pointY = Math.cos(angleMultiplier) * (distanceToCenter + deviation.values[i].y); // Y
            var point = {
                x: pointX,
                y: pointY + _this.windowHeight / 2
            };

            // Push point to array
            circleBase.push(point);

            // Redefines deviation
            deviation.definer();

            // Redefines variables for the outer circle
            angleMultiplier = toRad(((i * divider) + halfRotation + stripeRotation + initialRotation) % 360); // Redefines the angle to project the point
            distanceToCenter = initialSpace + circleWidth + deviation.values[i].distance; // Redefines the distance to the center
            pointX = Math.sin(angleMultiplier) * (distanceToCenter + deviation.values[i].x); // X
            pointY = Math.cos(angleMultiplier) * (distanceToCenter + deviation.values[i].y); // Y
            point = {
                x: pointX,
                y: pointY + _this.windowHeight / 2
            };


            // Push point to array
            circleOut.push(point);
        };

        circlesBase.push(circleBase);
        circlesOut.push(circleOut);
    }

    // Draw the lines
    // this.windowContext.lineWidth = 9;
    this.windowContext.beginPath();
    for (cs = 0; cs < circlesOut.length; cs++) { // For each outside circle
        for (var i = 0; i < circlesOut[cs].length; i++) { // For each point inside the circle
            var nextCircle = circlesOut[cs + 2];
            var pointDifference = -1;
            var circleLength = circlesOut[cs].length;

            this.windowContext.moveTo(circlesOut[cs][i].x, circlesOut[cs][i].y); // Selected point
            this.windowContext.lineTo(circlesBase[cs][i].x, circlesBase[cs][i].y); // No círculo anterior, o mesmo ponto
            this.windowContext.moveTo(circlesOut[cs][i].x, circlesOut[cs][i].y); // Selected point

            if (circlesBase[cs][i + 1]) { // Se o próximo pronto do círculo anterior existir
                this.windowContext.lineTo(circlesBase[cs][i + 1].x, circlesBase[cs][i + 1].y); // No círculo anterior, o próximo ponto
            } else {
                this.windowContext.lineTo(circlesBase[cs][0].x, circlesBase[cs][0].y); // No círculo anterior, o primeiro pronto
            };

            this.windowContext.moveTo(circlesOut[cs][i].x, circlesOut[cs][i].y); // Selected point

            // Drawing guides
            if (nextCircle) {
                if (i != 0) { // Não é o primeiro ponto
                    this.windowContext.lineTo(nextCircle[i + pointDifference].x, nextCircle[i + pointDifference].y);
                } else {
                    this.windowContext.lineTo(nextCircle[circleLength - 1].x, nextCircle[circleLength - 1].y);
                }
            }
        };
    };

    // Make it appear
    this.windowContext.stroke();

    // Animation
    if (this.updating) {
        requestAnimationFrame(this.uaePattern22.bind(this));
    }
}
