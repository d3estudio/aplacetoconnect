// Replicates the tile all over the window
patternWindow.prototype.worldPatterns = function() {
    // Dimensions for the tile canvas
    if (this.tileCanvas.width != this.tile.tileWidth || this.tileCanvas.height != this.tile.tileHeight) {
        this.tileCanvas.width = this.tile.tileWidth;
        this.tileCanvas.height = this.tile.tileHeight;
    }

    // Clear both canvas
    this.windowContext.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.tileContext.clearRect(0, 0, this.tileCanvas.width, this.tileCanvas.height);

    // Set canvas style
    this.worldPatternsSetStyle();

    // Updates tile
    this.tileUpdate(this.params.longitude, this.params.longitudeMin, this.params.longitudeS, this.params.latitude, this.params.latitudeMin, this.params.latitudeS);

    // Replicates the tile
    this.tile.replicator.bind(this)();

    // TWEEN
    TWEEN.update();

    // Will not pass when the tween is complete
    if (this.updating) {
        requestAnimationFrame(this.worldPatterns.bind(this));
    } // Call another frame
};

// Redefines every segment on each frame
patternWindow.prototype.tileUpdate = function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS) {
    var multiplierX = this.tile.tileWidth / 100.0, // Used to draw the tile proportionally to its size
        multiplierY = this.tile.tileHeight / 100.0, // Used to draw the tile proportionally to its size
        points = []; // For the threshold method
    lines = this.tile.lines(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS), // Stores every line
        thresholdRate = this.tile.thresholdRate;

    // Makes each segment relative to the tile width and draw them
    for (i = 0; i < lines.length; i++) {
        for (s = 0; s < lines[i].length; s++) {
            // Make the relative
            lines[i][s][0] *= multiplierX;
            lines[i][s][1] *= multiplierY;

            // Push all points to the variable "points"
            // that will be analized by the threshold
            var point = [lines[i][s][0], lines[i][s][1]];
            points.push(point);
        };
    };

    // Threshold
    if (thresholdRate != 0) { // Only if there is any kind of threshold
        for (i = 0; i < points.length; i++) { // For every point in the array
            for (s = 0; s < points.length; s++) { // Compares this to all the others
                var diffX = points[i][0] - points[s][0], // X difference between them
                    diffY = points[i][1] - points[s][1]; // Y difference between them

                if (diffX < thresholdRate && diffX > thresholdRate * -1 && diffX != 0) {
                    var averageX = (points[i][0] + points[s][0]) / 2;

                    points[i][0] = averageX;
                    points[s][0] = averageX;
                }

                if (diffY < thresholdRate && diffY > thresholdRate * -1 && diffY != 0) {
                    var averageY = (points[i][1] + points[s][1]) / 2;

                    points[i][1] = averageY;
                    points[s][1] = averageY;
                }
            };
        };
    }

    // Redefines lines arrays after threshold proccess
    for (i = 0; i < lines.length; i++) {
        lines[i] = points.slice(0, lines[i].length);
        points.splice(0, lines[i].length);
    };

    // Draw lines
    for (i = 0; i < lines.length; i++) {
        for (s = 0; s < lines[i].length; s++) {
            if (s == 0) { // If it is a new line
                this.tileContext.stroke(); // Stroke the last one
                this.tileContext.beginPath(); // Begin another
                this.tileContext.moveTo(lines[i][s][0], lines[i][s][1]);
            } else {
                this.tileContext.lineTo(lines[i][s][0], lines[i][s][1]);
            }
        };
    };
};
