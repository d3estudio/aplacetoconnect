patternWindow.prototype.uaePattern9 = function() {
    var width = this.windowWidth;

    // Update Tween
    TWEEN.update();

    // Set  building values
    var spaceDifferenceBase = width / 50 + map(this.params.latitudeMin, 0, 60, -width / 100, width / 100);
    var spaceDifferenceBetweenLines = width / 16.666666667 + map(this.params.latitudeS, 0, 60, width / 50, width / 28.571428571);
    var initialMargin = width / 25 + map(this.params.latitude, 22, 27, width / 20, width / 10);
    var afterRotation = toRad(0 + map(this.params.longitudeMin, 0, 60, 0, 10));
    var coordinateDeviation = map(this.params.longitudeS, 0, 60, -width / 3.3333333, width / 10);
    var rotationVariation = map(this.params.longitude, 51, 57, 35, 45);

    // Fixed value
    var repetitionNumber = 5;
    var circlesNumber = 20;
    var x = width * 2;
    var y = x + coordinateDeviation;

    // Set canvas style
    this.setStyle();

    // Clear canvas
    this.windowContext.clearRect(0, 0, width, 900);

    this.windowContext.beginPath();
    for (s = 0; s < repetitionNumber; s++) {
        for (var i = 0; i < circlesNumber; i++) {
            var point1 = {
                x: -x,
                y: y
            };
            var point2 = {
                x: 0,
                y: 0
            };
            var point3 = {
                x: x,
                y: y
            };

            var translationY = s * spaceDifferenceBase + (i * spaceDifferenceBetweenLines);
            var translation = (-initialMargin) + (-translationY);
            var rotation1 = toRad((i * 4) + (s * rotationVariation));

            // Rotation1
            var initialAngle = Math.atan(x / y);
            var newAngle = initialAngle - rotation1;
            var hipotenuse = Math.sqrt((x * x) + (y * y));
            var newX = Math.sin(newAngle) * hipotenuse;
            var newY = Math.cos(newAngle) * hipotenuse;

            point3.x = newX;
            point3.y = newY;

            var initialAngle2 = Math.atan(-x / y);
            var newAngle2 = initialAngle2 - rotation1;
            var newX2 = Math.sin(newAngle2) * hipotenuse;
            var newY2 = Math.cos(newAngle2) * hipotenuse;

            point1.x = newX2;
            point1.y = newY2;

            // Translation1
            var hipotenuse2 = -translation; // Negative
            var translation1X = Math.sin(rotation1) * hipotenuse2;
            var translation1Y = Math.cos(rotation1) * hipotenuse2;

            point1.x += translation1X;
            point1.y -= translation1Y;
            point2.x += translation1X;
            point2.y -= translation1Y;
            point3.x += translation1X;
            point3.y -= translation1Y;

            // Rotation 2
            var compensationX = point2.x;
            var compensationY = point2.y;

            var catetoOposto = point3.x - point2.x;
            var catetoAdjacente = point3.y - point2.y;
            var angle1 = Math.atan(catetoOposto / catetoAdjacente);
            var newAngle3;

            if (point2.y > point3.y) {
                newAngle3 = toRad(-180) + (angle1 - afterRotation);
            } else {
                newAngle3 = angle1 - afterRotation;
            }

            var newX3 = (Math.sin(newAngle3) * hipotenuse) + compensationX;
            var newY3 = (Math.cos(newAngle3) * hipotenuse) + compensationY;

            point3.x = newX3;
            point3.y = newY3;

            var catetoOposto2 = point1.x - point2.x;
            var catetoAdjacente2 = point1.y - point2.y;
            var angle2 = Math.atan(catetoOposto2 / catetoAdjacente2);

            var newAngle4;

            if (point2.y < point1.y) {
                newAngle4 = angle2 - afterRotation;
            } else {
                newAngle4 = toRad(-180) + (angle2 - afterRotation);
            }

            var newX4 = (Math.sin(newAngle4) * hipotenuse) + compensationX;
            var newY4 = (Math.cos(newAngle4) * hipotenuse) + compensationY;

            point1.x = newX4;
            point1.y = newY4;

            // Screen middle
            point1.y += this.windowHeight / 2;
            point2.y += this.windowHeight / 2;
            point3.y += this.windowHeight / 2;

            // Draw
            this.windowContext.moveTo(point1.x, point1.y);
            this.windowContext.lineTo(point2.x, point2.y);
            this.windowContext.lineTo(point3.x, point3.y);
        };
    };
    this.windowContext.stroke();

    if (this.updating) {
        requestAnimationFrame(this.uaePattern9.bind(this));
    }
};
