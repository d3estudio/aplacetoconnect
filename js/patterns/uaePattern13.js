patternWindow.prototype.uaePattern13 = function() {
    var width = this.windowWidth;
    var widthRelation = 1000 / width;

    // Update Tween
    TWEEN.update();

    // Base variables
    var longitude = this.params.longitude;
    var longitudeMin = this.params.longitudeMin;
    var longitudeS = this.params.longitudeS;
    var latitude = this.params.latitude;
    var latitudeMin = this.params.latitudeMin;
    var latitudeS = this.params.latitudeS;

    var initialMargin = map(longitudeS, 0, 60, width / 25, width / 8.333333);
    var rotationAngle3Base = map(latitudeS, 0, 60, 0, 15);
    var distanceBase = map(latitudeMin, 0, 60, width / 33.33333, width / 20);
    var distanceDecrease = map(longitudeMin, 0, 60, width / 200, width / 100);
    var initialPositionDeviation = map(longitude, 51, 57, 0, width / 100);
    var rotation1Deviation = map(latitude, 22, 27, -2, 2);

    // Set canvas style
    this.setStyle();

    this.windowContext.clearRect(0, 0, width, 900);

    this.windowContext.beginPath();
    for (s = 0; s < 10; s++) {
        for (i = 0; i < 60; i++) {
            var point1 = [0, 0];
            var point2 = [width / 2, width / 2];

            var distance = distanceBase - (i / widthRelation) / distanceDecrease;

            // Screen middle
            point1[1] += this.windowHeight / 2 + i * initialPositionDeviation;
            point2[1] += this.windowHeight / 2 + i * initialPositionDeviation;

            // Rotation 1
            var catetoOposto1 = point1[0] + point2[0];
            var catetoAdjacente1 = point1[1] + point2[1];
            var hipotenusa1 = Math.sqrt((catetoOposto1 * catetoOposto1) + (catetoAdjacente1 * catetoAdjacente1));
            var rotationAngle1 = toRad(45 - (s * 36));

            point2[0] = Math.sin(rotationAngle1) * hipotenusa1;
            point2[1] = Math.cos(rotationAngle1) * hipotenusa1;

            // Translation 1
            var rotationAngle2 = toRad((s * 36) + i / 5 * rotation1Deviation);
            var hipotenusa2 = initialMargin + (distance * i);
            var translationY = Math.sin(rotationAngle2) * hipotenusa2;
            var translationX = Math.cos(rotationAngle2) * hipotenusa2;

            point1[0] += translationX;
            point1[1] += translationY;
            point2[0] += translationX;
            point2[1] += translationY;

            // Rotation 2
            var catetoOposto2 = point2[0] - point1[0];
            var catetoAdjacente2 = point2[1] - point1[1];
            var hipotenusa3 = Math.sqrt((catetoOposto2 * catetoOposto2) + (catetoAdjacente2 * catetoAdjacente2));
            var rotationAngle3 = toRad(rotationAngle3Base - (s * 36));

            point2[0] = Math.sin(rotationAngle3) * hipotenusa3 + point1[0];
            point2[1] = Math.cos(rotationAngle3) * hipotenusa3 + point1[1];

            this.windowContext.moveTo(point1[0], point1[1]);
            this.windowContext.lineTo(point2[0], point2[1]);
        };
    };
    this.windowContext.stroke();

    // If it needs another update, call the function again
    if (this.updating) {
        requestAnimationFrame(this.uaePattern13.bind(this));
    }
};
