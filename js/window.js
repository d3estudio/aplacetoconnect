/* D3! Digital Producer */
/* CANVAS SCRIPT */

// The big bang!
patternWindow = function(canvasId, canvasWidth) {
    var d = document;

    // Basic
    this.strokeColor = '#fff'

    // Get a reference to the canvas object
    this.windowCanvas = d.getElementById(canvasId);
    this.windowContext = this.windowCanvas.getContext("2d");

    // Canvas sizes
    this.windowWidth = canvasWidth;
    // Obs: all the final vectors were generated with a 1000 width
    // To keep consistency, it's recomended to use this value again for other material creations
    // Because the canvas resize can cause some visual differences
    this.windowHeight = (this.windowWidth / 10) * 9;

    this.linewidth = Math.round(this.windowWidth / 111.11111111);;

    // Basic setup for first render
    this.params = {
        longitude: 0,
        longitudeMin: 0,
        longitudeS: 0,
        latitude: 0,
        latitudeMin: 0,
        latitudeS: 0
    };
    this.targetParameters = {
        longitude: 0,
        longitudeMin: 0,
        longitudeS: 0,
        latitude: 0,
        latitudeMin: 0,
        latitudeS: 0,
        quadrant: 0
    }

    // Animation control
    this.animationTime = 800;

    // Dimensions for the window canvas
    this.windowCanvas.width = this.windowWidth;
    this.windowCanvas.height = this.windowHeight;

    // Create a new canvas only for the world patterns tile
    this.tileCanvas = d.createElement("canvas");
    this.tileContext = this.tileCanvas.getContext("2d");

    // UAE
    // New canvas for the whole pattern1
    this.uaeIntermedianteCanvas = d.createElement("canvas");
    this.uaeIntermediateContext = this.uaeIntermedianteCanvas.getContext("2d");

    // New canvas for master tile
    this.uaeMasterTilesCanvas = d.createElement("canvas");
    this.uaeMasterTilesContext = this.uaeMasterTilesCanvas.getContext("2d");

    // Create a new canvas only for the uae tile
    this.uaeTilesCanvas = d.createElement("canvas");
    this.uaeTilesContext = this.uaeTilesCanvas.getContext("2d");

    //uaeTileWidth = uaeTileHeight = 350;
    this.uaeTileWidth = this.uaeTileHeight = this.windowWidth / 2.5;
    this.radiusUnit = this.uaeTileWidth;

    // Dimensions for the other canvas
    this.uaeTilesCanvas.width = this.uaeTileWidth;
    this.uaeTilesCanvas.height = this.uaeTileHeight;
    this.uaeMasterTilesCanvas.width = this.uaeTileWidth;
    this.uaeMasterTilesCanvas.height = this.uaeTileHeight;
    this.uaeIntermedianteCanvas.width = this.windowWidth;
    this.uaeIntermedianteCanvas.height = this.windowHeight;

    // Define size multiplier
    this.sizeMultiplier = this.uaeTileWidth / 100;

    // Set style
    this.uaeTilesContext.strokeStyle = this.strokeColor;
    this.uaeTilesContext.fillStyle = "#f6f4e9"
    this.uaeTilesContext.lineWidth = this.linewidth;
    this.uaeTilesContext.lineJoin = "miter";
    this.uaeTilesContext.miterLimit = 1;
};

// Set the style for the canvas
patternWindow.prototype.setStyle = function() {
    this.windowContext.strokeStyle = this.strokeColor;
    this.windowContext.lineWidth = this.linewidth;
    this.windowContext.lineJoin = "miter";
    this.windowContext.miterLimit = 1;
    this.windowContext.lineCap = "butt";
};

patternWindow.prototype.worldPatternsSetStyle = function() {
    // Visuals
    this.tileContext.strokeStyle = this.strokeColor;
    this.tileContext.lineJoin = "miter";
    this.tileContext.lineWidth = this.linewidth;
    this.tileContext.miterLimit = 2;
}

// Evaluete the relation between empty and filled spaces in the canvas
patternWindow.prototype.evaluateColors = function() {
    var imgd = this.windowContext.getImageData(0, 0, this.windowWidth, this.windowHeight);
    var pix = imgd.data;
    var counter = 0;

    // Loop over each pixel and invert the color.
    for (var i = 0, n = pix.length; i < n; i += 4) {
        if (pix[i] != 0 && pix[i + 1] != 0 && pix[i + 2] != 0) {
            counter++;
        }
    }

    console.log('');
    console.log('Color evaluation:' + counter + "/" + (this.windowWidth * this.windowHeight));
    console.log("-> " + Math.round(counter / ((this.windowWidth * this.windowHeight) / 100)) + "%");
};

// Starts the transform animations
patternWindow.prototype.play = function(functionToCall) {
    var _this = this;

    if (this.updating) {
        TWEEN.removeAll();
    }

    this.updating = true;

    // Test only
    // this.targetParameters = {
    //  longitude: parseInt($('input.longitude').val()),
    //  longitudeMin: parseInt($('input.longitudeMin').val()),
    //  longitudeS: parseInt($('input.longitudeSeg').val()),
    //  latitude: parseInt($('input.latitude').val()),
    //  latitudeMin: parseInt($('input.latitudeMin').val()),
    //  latitudeS: parseInt($('input.latitudeSeg').val())
    // };

    var uaeAnimationTween = new TWEEN.Tween(this.params)
        .to({
            longitude: this.targetParameters.longitude,
            longitudeMin: this.targetParameters.longitudeMin,
            longitudeS: this.targetParameters.longitudeS,
            latitude: this.targetParameters.latitude,
            latitudeMin: this.targetParameters.latitudeMin,
            latitudeS: this.targetParameters.latitudeS
        }, this.animationTime)
        .easing(TWEEN.Easing.Cubic.Out) // Ease
        .onComplete(function() {
            _this.updating = false;
        })
        .start(); // Play!

    requestAnimationFrame(functionToCall.bind(this));
};

// Updates window
patternWindow.prototype.update = function(position) {
    this.targetParameters.longitude = parseInt(position.lng[0]),
        this.targetParameters.longitudeMin = parseInt(position.lng[1]),
        this.targetParameters.longitudeS = parseInt(position.lng[2]),
        this.targetParameters.latitude = parseInt(position.lat[0]),
        this.targetParameters.latitudeMin = parseInt(position.lat[1]),
        this.targetParameters.latitudeS = parseInt(position.lat[2]);
    this.targetParameters.quadrant = parseInt(position.quadrant);
    this.targetParameters.UAEEmirate = position.UAEEmirate;

    var emirate = this.targetParameters.UAEEmirate;

    // Select pattern to use
    if (emirate != undefined) { // If its an emirate
        if (emirate == "Dubai") {
            this.play(this.uaePattern1);
            return;
        } else if (emirate == "Sharjah") {
            this.play(this.uaePattern9);
            return;
        } else if (emirate == "Ajman") {
            this.play(this.uaePattern13);
            return;
        } else if (emirate == "Umm Al Quwain") {
            this.play(this.uaePattern20);
            return;
        } else if (emirate == "Ras al Khaimah") {
            this.play(this.uaePattern2);
            return;
        } else if (emirate == "Fujairah") {
            this.play(this.uaePattern22);
            return;
        } else if (emirate == "Abu Dhabi") {
            this.play(this.uaePattern4);
            return;
        };
    } else {
        // Defines which tile to be rendered
        // _______________________
        // |1         |2         |
        // |     2    |     3    |  <- World map and its tiles
        // |__________|__________|
        // |3         |4         |
        // |     4    |     1    |
        // |__________|__________|
        switch (this.targetParameters.quadrant) {
            case 1:
                this.tile = this.tile2;
                break;
            case 2:
                this.tile = this.tile3;
                break;
            case 3:
                this.tile = this.tile4;
                break;
            case 4:
                this.tile = this.tile1;
                break;
            default:
                this.tile = this.tile4; // Only for first render
        }

        this.play(this.worldPatterns);
    }
};
