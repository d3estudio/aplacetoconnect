/* D3! Digital Producer */
/* MAP SCRIPT */

windowMap = function(city, mapDivId, windowToUpdate, coordinatesToUpdate) {
    var _this = this;

    // Map style definition
    var style = [{
        featureType: "administrative",
        elementType: "geometry",
        stylers: [{
            color: "#a7a7a7"
        }]
    }, {
        featureType: "administrative",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "administrative.province",
        elementType: "geometry.stroke",
        stylers: [{
            visibility: "on"
        }, {
            weight: "0.5"
        }]
    }, {
        featureType: "administrative",
        elementType: "labels.text.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#737373"
        }]
    }, {
        featureType: "administrative.country",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            saturation: "0"
        }]
    }, {
        featureType: "administrative.country",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "administrative.province",
        elementType: "geometry.fill",
        stylers: [{
            saturation: "0"
        }]
    }, {
        featureType: "administrative.province",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "administrative.locality",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "administrative.neighborhood",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "administrative.land_parcel",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "landscape",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#d3d3d3"
        }]
    }, {
        featureType: "landscape",
        elementType: "labels",
        stylers: [{
            visibility: "on"
        }]
    }, {
        featureType: "landscape.man_made",
        elementType: "geometry",
        stylers: [{
            lightness: "0"
        }]
    }, {
        featureType: "poi",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#dadada"
        }]
    }, {
        featureType: "poi",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "poi",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
            lightness: "0"
        }]
    }, {
        featureType: "road",
        elementType: "geometry.fill",
        stylers: [{
            lightness: "0"
        }]
    }, {
        featureType: "road",
        elementType: "labels",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "road",
        elementType: "labels.text.fill",
        stylers: [{
            color: "#696969"
        }]
    }, {
        featureType: "road",
        elementType: "labels.icon",
        stylers: [{
            visibility: "off"
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry.fill",
        stylers: [{
            color: "#eeeeee"
        }]
    }, {
        featureType: "road.highway",
        elementType: "geometry.stroke",
        stylers: [{
            visibility: "on"
        }, {
            color: "#ababab"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.fill",
        stylers: [{
            color: "#eeeeee"
        }]
    }, {
        featureType: "road.arterial",
        elementType: "geometry.stroke",
        stylers: [{
            color: "#ababab"
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry",
        stylers: [{
            lightness: "0"
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry.fill",
        stylers: [{
            visibility: "on"
        }, {
            color: "#eeeeee"
        }, {
            weight: 1.8
        }]
    }, {
        featureType: "road.local",
        elementType: "geometry.stroke",
        stylers: [{
            color: "#ababab"
        }]
    }, {
        featureType: "transit",
        elementType: "all",
        stylers: [{
            color: "#808080"
        }, {
            visibility: "off"
        }]
    }, {
        featureType: "water",
        elementType: "geometry.fill",
        stylers: [{
            color: "#989898"
        }]
    }, {
        featureType: "water",
        elementType: "geometry.stroke",
        stylers: [{
            lightness: "0"
        }]
    }];

    // Times
    this.updateSocketTime = 500; //milesoconds
    this.standByTime = 1000 * 60; //milesoconds

    // Navigation limits
    this.boundSouthWestLat = -85;
    this.boundNorthWestLng = -180;
    this.boundNorthEastLat = 85;
    this.boundSouthEastLng = 180;
    this.minZoomLevel = 4;
    this.maxZoomLevel = 15;

    // Creation properties
    this.mapDivId = mapDivId;
    this.windowToUpdate = windowToUpdate;
    this.coordinatesToUpdate = coordinatesToUpdate;

    // Other
    this.sendUpdateTimeout = false;
    this.refreshRate = 200;
    this.selScrollable = '.scrollable';
    this.updating = false;
    this.standByTimeout = false;
    this.initialZoomLevel = 10;
    this.position;

    // Selects initial position
    if (city == 'Dubai') { // If Dubai
        this.initialLat = 25.197629;
        this.initialLng = 55.271703;
    } else if (city == 'Sharjah') { // If Sharjah
        this.initialLat = 25.35824;
        this.initialLng = 55.38158;
    };

    // Set initial position
    this.myLatlng = new google.maps.LatLng(this.initialLat, this.initialLng);

    // Set options
    var mapOptions = {
        center: this.myLatlng,
        disableDefaultUI: true, // Test only, after set to true
        zoom: this.initialZoomLevel
    };

    // Map creating
    this.map = new google.maps.Map(document.getElementById(this.mapDivId), mapOptions); // Creates map
    this.map.set('styles', style); // Apply style

    // Create listeners
    this.createEventListeners();

    // Create borders
    this.createBorders();

    // Create maps if needed
    if (window.maps == undefined) {
        window.maps = []; // Creates array
    };

    // Push to maps
    maps.push(this);

    // Calls an update
    this.updateLatLng(mapDivId);
};

// Events
windowMap.prototype.createEventListeners = function() {
    var _this = this;

    google.maps.event.addListener(this.map, 'dragstart', function() {
        _this.clearTimeouts();
    });

    google.maps.event.addListener(this.map, 'dragend', function() {
        _this.clearTimeouts();
    });

    google.maps.event.addListener(this.map, 'drag', function() {
        _this.clearTimeouts();
    });

    google.maps.event.addListener(this.map, 'center_changed', function() {
        _this.testBounds();
        _this.updateLatLng(_this.mapDivId);
        _this.clearTimeouts();
    });

    google.maps.event.addListener(this.map, 'zoom_changed', function() {
        _this.clearTimeouts();

        // Test for current animation
        if (_this.updating) return;

        // If its not animating, check the zoom level
        if (_this.map.getZoom() < _this.minZoomLevel) {
            _this.map.setZoom(_this.minZoomLevel)
        } else if (_this.map.getZoom() > _this.maxZoomLevel) {
            _this.map.setZoom(_this.maxZoomLevel)
        }

        _this.updateLatLng(_this.mapDivId);
    });
};

windowMap.prototype.updateLatLng = function(mapDivId) {
    var mapPosition = this.map.getCenter(),
        _lat = mapPosition.lat(),
        _lng = mapPosition.lng(),
        _quadrant = 0,
        _latD,
        _lngD;

    // Longitude value normalization
    if (_lng < 0) { // Moved to left
        if (Math.abs(parseInt(_lng / -180)) % 2 > 0) { // E or W
            //W
            _lng = 180 - Math.abs(_lng % -180);
        } else {
            //E
            _lng = _lng - (Math.floor(_lng / -180) * -180);
        }
    } else { // Moved to right
        if (Math.abs(parseInt(_lng / 180)) % 2 > 0) { // E or W
            //W
            _lng = (_lng - (Math.floor(_lng / 180) + 1) * 180);
        } else {
            //E
            _lng = (_lng - Math.floor(_lng / 180) * 180);
        }
    }

    // Quadrant definition
    if (_lat > 0) {
        if (_lng < 0) {
            _quadrant = 1;
            _latD = 'N';
            _lngD = 'W';
        } else {
            _quadrant = 2;
            _latD = 'N';
            _lngD = 'E';
        }
    } else {
        if (_lng < 0) {
            _quadrant = 3;
            _latD = 'S';
            _lngD = 'W';
        } else {
            _quadrant = 4;
            _latD = 'S';
            _lngD = 'E';
        }
    }

    // Defines position to be broadcasted
    this.position = {
        quadrant: _quadrant,
        lat: decToDeg('lat', _quadrant, _lat),
        lng: decToDeg('lng', _quadrant, _lng)
    };

    // Refresh map coordinates
    $('#' + this.coordinatesToUpdate).attr('data-content', decToDegFormatted('lat', _quadrant, _lat) + _latD + ' ' + decToDegFormatted('lng', _quadrant, _lng) + _lngD);
}

windowMap.prototype.clearTimeouts = function() {
    var _this = this;

    clearTimeout(this.sendUpdateTimeout);
    this.sendUpdateTimeout = setTimeout(_this.sendUpdate.bind(_this), _this.updateSocketTime);

    for (var i = 0; i < maps.length; i++) {
        clearTimeout(maps[i].standByTimeout);
        maps[i].standByTimeout = setTimeout(mapsToOrigin, maps[i].standByTime);
    };
};

windowMap.prototype.testBounds = function() {
    var c = this.map.getCenter(),
        x = c.lng(),
        y = c.lat();

    if (y < this.boundSouthWestLat) y = this.boundSouthWestLat;
    if (y > this.boundNorthEastLat) y = this.boundNorthEastLat;

    if (c.lng() !== x || c.lat() !== y)
        this.map.panTo(new google.maps.LatLng(y, x));
};

windowMap.prototype.sendUpdate = function() {
    var mapPosition = this.map.getCenter();

    var _this = this;
    var latlng = new google.maps.LatLng(mapPosition.lat(), mapPosition.lng());
    var result;

    // Find out in wich emirate is the center of the map
    if (this.dubaiBorders.containsLatLng(latlng) || this.dubaiEastBorders.containsLatLng(latlng)) {
        result = 'Dubai';
    } else if (this.abuDhabiBorders.containsLatLng(latlng)) {
        result = 'Abu Dhabi';
    } else if (this.ajmanBorders.containsLatLng(latlng) || this.centralAjmanBorders.containsLatLng(latlng)) {
        result = 'Ajman';
    } else if (this.sharjahBorders.containsLatLng(latlng) || this.sharjahEastBorders.containsLatLng(latlng)) {
        result = 'Sharjah';
    } else if (this.ummAlKhaimanBorders.containsLatLng(latlng)) {
        result = "Umm Al Quwain";
    } else if (this.rasAlKhaimanBorders.containsLatLng(latlng) || this.rasAlKhaimanSouthBorders.containsLatLng(latlng)) {
        result = "Ras al Khaimah";
    } else if (this.fujairaBorders.containsLatLng(latlng) || this.fujairaSouthBorders.containsLatLng(latlng)) {
        result = "Fujairah";
    }

    // Send the emirate with the position object
    this.position.UAEEmirate = result;

    // Calls the update
    this.windowToUpdate.update(this.position);
    clearTimeout(_this.sendUpdateTimeout);
};
