var dubaiWindow;
var dubaiMap;
var sharjahWindow;
var sharjahMap;

// Starts everythinh
$(document).ready(function() {
    var canvasWidth = 500;

    // Prevent overscroll on ipad
    $(document).on('touchmove', function(e) {
        e.preventDefault();
    });

    // Resize canvas
    $(window).resize(resizeCanvas);

    $(window).load(function() {
        // Creates dubai window
        dubaiWindow = new patternWindow('dubaiWindow', canvasWidth);
        dubaiMap = new windowMap('Dubai', 'dubaiMap', dubaiWindow, 'dubaiCoordinates');

        // Creates Sharjah window
        sharjahWindow = new patternWindow('sharjahWindow', canvasWidth);
        sharjahMap = new windowMap('Sharjah', 'sharjahMap', sharjahWindow, 'sharjahCoordinates');

        // "Trigger" first render
        sharjahMap.updateLatLng(sharjahMap.mapDivId);
        dubaiMap.updateLatLng(dubaiMap.mapDivId);
        sharjahMap.clearTimeouts();
        dubaiMap.clearTimeouts();

        // Triggers the canvas resize
        $(window).trigger('resize');
    });
});

// Same thing as Processing map function
function map(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
};

// Convert degress to radians
function toRad(degree) {
    return degree * Math.PI / 180;
};

// Convert radians to degrees
function toDeg(rad) {
    return rad * (180 / Math.PI);
};

// Put both window.maps in origin
function mapsToOrigin() {
    for (i = 0; i < window.maps.length; i++) {
        var mapGoingToOrigin = window.maps[i];

        var c = mapGoingToOrigin.map.getCenter(),
            x = c.lng(),
            y = c.lat();

        if (Math.round(x * 10000) / 10000 != Math.round(mapGoingToOrigin.initialLng * 10000) / 10000 && Math.round(y * 10000) / 10000 != Math.round(mapGoingToOrigin.initialLat * 10000) / 10000) {
            mapGoingToOrigin.map.panTo(new google.maps.LatLng(mapGoingToOrigin.initialLat, mapGoingToOrigin.initialLng));

            setTimeout(function() {
                mapGoingToOrigin.map.setZoom(mapGoingToOrigin.initialZoomLevel);
            }, 400);
        }
    };
};

// Convert map position
function decToDegFormatted(latOrLng, quadrant, dec) {
    var data = decToDeg(latOrLng, quadrant, dec);

    return Math.abs(data[0]) + "°" + data[1] + "'" + data[2] + "\"";
}

// Decompose position
function decToDeg(latOrLng, quadrant, dec) {
    var str = "",
        deg = 0,
        mnt = 0,
        sec = 0;

    dec = Math.abs(dec);
    deg = Math.floor(dec);
    dec = (dec - Math.floor(dec)) * 60;
    mnt = Math.floor(dec);
    dec = (dec - Math.floor(dec)) * 60;
    sec = Math.floor(dec * 100) / 100;

    if ((latOrLng == 'lat' && (quadrant == 3 || quadrant == 4)) || (latOrLng == 'lng' && (quadrant == 1 || quadrant == 3))) {
        deg = deg * -1;
    }

    return Array(deg, mnt, parseInt(sec));
}

// Resize canvas
function resizeCanvas() {
    var canvasDimensionBase = $(window).height() / 2;

    // Dimensions for the canvas
    $('canvas').width(canvasDimensionBase);
    $('canvas').height((canvasDimensionBase / 10) * 9);

    // Dimensions for the canvas container
    $('.canvasContainer').width($('canvas').height() + 20);
}
